package com.ksundaysky.agileboard;

//import com.ksundaysky.agileboard.configuration.H2JpaConfig;
//import com.ksundaysky.agileboard.configuration.H2JpaConfig;
import com.ksundaysky.agileboard.configuration.H2JpaConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

//
//@RunWith(SpringRunner.class)
//@SpringBootTest
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AgileBoardApplication.class, H2JpaConfig.class})
@ActiveProfiles("test")
public class AgileBoardApplicationTests {


	@Configuration
	@EnableJpaRepositories(basePackages = "com.ksundaysky.agileboard.repositories")
	@PropertySource(value = {"classpath:persistence-generic-entity.properties"})
	@EnableTransactionManagement
	static class H2JpaConfig {

		@Autowired
		private Environment env;

		@Bean
		public DataSource dataSource() {
			DriverManagerDataSource dataSource = new DriverManagerDataSource();
			dataSource.setUrl(env.getProperty("jdbc.url"));
			dataSource.setUsername(env.getProperty("jdbc.user"));
			dataSource.setPassword(env.getProperty("jdbc.pass"));

			return dataSource;
		}
	}
	@Test
	public void contextLoads() {
	}

}
