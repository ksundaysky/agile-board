package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.EmployeeCommand;
import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.commands.TeamCommand;
import com.ksundaysky.agileboard.domain.Employee;
import com.ksundaysky.agileboard.domain.Project;
import com.ksundaysky.agileboard.domain.Role;
import com.ksundaysky.agileboard.domain.Team;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class EmployeeCommandToEmployeeTest {

    EmployeeCommandToEmployee converter;
    @Before
    public void setUp() throws Exception {
        converter = new EmployeeCommandToEmployee();
    }


    @Test
    public void testNullObject()  {
        assertNull(converter.convert(null));
    }

//    @Test
//    public void testEmptyObject() throws Exception {
//        assertNotNull(converter.convert(new EmployeeCommand()));
//    }

    @Test
    public void convert(){
        //given
        EmployeeCommand employeeCommand = new EmployeeCommand();
        employeeCommand.setId(1L);
        employeeCommand.setProjects(new HashSet<ProjectCommand>());
        employeeCommand.setPesel("11");
        employeeCommand.setLastName("Sundaysky");
        employeeCommand.setFirstName("Chrisopher");
        employeeCommand.setEmail("admin@admin.pl");
        employeeCommand.setRole("DEVELOPER");
        employeeCommand.setTeams(new HashSet<TeamCommand>());

        //when
        Employee employee = converter.convert(employeeCommand);

        //then
        assertEquals(employeeCommand.getPesel(),employee.getPesel());
        assertEquals(employeeCommand.getId(),employee.getId());
//        assertEquals(employeeCommand.getRole(),employee.getRole());

    }
}