package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.EmployeeCommand;
import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.commands.TaskCommand;
import com.ksundaysky.agileboard.domain.Project;
import com.ksundaysky.agileboard.domain.Task;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TaskCommandToTaskTest {

    TaskCommandToTask converter;

    @Before
    public void setUp() throws Exception {
        converter = new TaskCommandToTask();
    }

    @Test
    public void convert() throws Exception {

        //given

        TaskCommand taskCommand = new TaskCommand();
        taskCommand.setId(1L);
        taskCommand.setName("AWS connection");
        taskCommand.setProject(new Project());
        taskCommand.setEmployee(null);
        taskCommand.setDateWhenDone("12-12-1233");

        //when

        Task task  = converter.convert(taskCommand);

        //then

        assertEquals(task.getId(),taskCommand.getId());
        assertEquals(task.getName(),taskCommand.getName());
        assertEquals(task.getEmployee(),taskCommand.getEmployee());
    }

    @Test
    public void testNullObject()  {
        assertNull(converter.convert(null));
    }

//    @Test
//    public void testEmptyObject() throws Exception {
//        assertNotNull(converter.convert(new TaskCommand()));
//    }

}