package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.commands.TeamCommand;
import com.ksundaysky.agileboard.domain.Project;
import com.ksundaysky.agileboard.domain.Team;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ProjectToProjectCommandTest {
    ProjectToProjectCommand converter;

    @Before
    public void setUp() throws Exception {
        converter = new ProjectToProjectCommand();
    }

    @Test
    public void testNullObject()  {
        assertNull(converter.convert(null));
    }

    @Test
    public void testEmptyObject() throws Exception {
        assertNotNull(converter.convert(new Project()));
    }

    @Test
    public void convert() throws Exception {

        //given
        Project project = new Project();
        project.setId(1L);
        project.setName("5G");
        Set teams = new HashSet<Team>();

        Team team1 = new Team();
        team1.setId(1l);
        team1.setName("pumba");
        team1.setDescription("cpp");
        teams.add(team1);

        project.setTeams(teams);

        //when

        ProjectCommand projectCommand = new ProjectCommand();
        projectCommand = converter.convert(project);

        //then

        assertEquals(project.getName(), projectCommand.getName());
        Set<TeamCommand> tempTeams = new HashSet<>();
        project.getTeams().forEach(team -> tempTeams.add(converter.teamConverter.convert(team)));
//        projectCommand.getTeams().forEach(team -> System.out.println(team.name));
//        tempTeams.forEach(team -> System.out.println(team.name));
//        assertEquals(projectCommand.getTeams(),tempTeams );
        assertEquals(project.getId(), projectCommand.getId());
    }

}