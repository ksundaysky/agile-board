package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.commands.TaskCommand;
import com.ksundaysky.agileboard.domain.Project;
import com.ksundaysky.agileboard.domain.Sprint;
import com.ksundaysky.agileboard.domain.Task;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;

import static org.junit.Assert.*;

public class TaskToTaskCommandTest {

    TaskToTaskCommand converter;
    @Before
    public void setUp() throws Exception {
        converter = new TaskToTaskCommand();
    }

    @Test
    public void testNullObject()  {
        assertNull(converter.convert(null));
    }
//
//    @Test
//    public void testEmptyObject() throws Exception {
//        assertNotNull(converter.convert(new Task()));
//    }

    @Test
    public void convert() throws Exception {
        //given

        Task task = new Task();
        task.setId(1L);
        task.setName("AWS connection");
        task.setProject(new Project());
        task.setEmployee(null);
        task.setSprint(new Sprint());
        task.setDateWhenDone(new java.sql.Date(213L));

        //when

        TaskCommand taskCommand  = converter.convert(task);

        //then

        assertEquals(task.getId(),taskCommand.getId());
        assertEquals(task.getName(),taskCommand.getName());
        assertEquals(task.getEmployee(),taskCommand.getEmployee());
    }

}