package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.EmployeeCommand;
import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.commands.TeamCommand;
import com.ksundaysky.agileboard.domain.Project;
import com.ksundaysky.agileboard.domain.Team;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ProjectCommandToProjectTest {

    ProjectCommandToProject converter;

    @Before
    public void setUp() throws Exception {
        converter = new ProjectCommandToProject();
    }


    @Test
    public void testNullObject()  {
        assertNull(converter.convert(null));
    }

    @Test
    public void testEmptyObject() throws Exception {
        assertNotNull(converter.convert(new ProjectCommand()));
    }
    @Test
    public void convert() throws Exception {

        //given
        ProjectCommand projectCommand = new ProjectCommand();
        projectCommand.setId(1L);
        projectCommand.setName("5G");
        Set teams = new HashSet<TeamCommand>();
        TeamCommand teamCommand1 = new TeamCommand();
        teamCommand1.setId(1l);
        teamCommand1.setName("pumba");
        teamCommand1.setDescription("cpp");
        teams.add(teamCommand1);

        projectCommand.setTeams(teams);

        //when

        Project project = new Project();
        project = converter.convert(projectCommand);

        //then

        assertEquals(project.getName(), projectCommand.getName());
        Set<Team> tempTeams = new HashSet<Team>();
        projectCommand.getTeams().forEach(teamCommand -> tempTeams.add(converter.teamConverter.convert(teamCommand)));
        assertEquals(project.getTeams(),tempTeams );
        assertEquals(project.getId(), projectCommand.getId());
    }

}