package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.TaskCommand;
import com.ksundaysky.agileboard.commands.TeamCommand;
import com.ksundaysky.agileboard.domain.Team;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TeamCommandToTeamTest {

    TeamCommandToTeam converter;
    @Before
    public void setUp() throws Exception {
        converter = new TeamCommandToTeam();
    }

    @Test
    public void testNullObject()  {
        assertNull(converter.convert(null));
    }

    @Test
    public void testEmptyObject() throws Exception {
        assertNotNull(converter.convert(new TeamCommand()));
    }

    @Test
    public void convert() throws Exception {

        //given

        TeamCommand teamCommand = new TeamCommand();
        teamCommand.setName("Pumba");
        teamCommand.setId(1l);

        //when

        Team team = converter.convert(teamCommand);

        //then

        assertEquals(team.getId(),teamCommand.getId());
        assertEquals(team.getName(),teamCommand.getName());
    }

}