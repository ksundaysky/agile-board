package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.EmployeeCommand;
import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.commands.TeamCommand;
import com.ksundaysky.agileboard.domain.Employee;
import com.ksundaysky.agileboard.domain.Project;
import com.ksundaysky.agileboard.domain.Role;
import com.ksundaysky.agileboard.domain.Team;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class EmployeeToEmployeeCommandTest {

    EmployeeToEmployeeCommand converter;

    @Before
    public void setUp() throws Exception {
        converter = new EmployeeToEmployeeCommand();
    }

    @Test
    public void testNullObject()  {
        assertNull(converter.convert(null));
    }

//    @Test
//    public void testEmptyObject() throws Exception {
//        assertNotNull(converter.convert(new Employee()));
//    }

    @Test
    public void convert() throws Exception {

        Employee employee = new Employee();
        employee.setId(1L);
        employee.setProjects(new HashSet<Project>());
        employee.setPesel("123456789112");
        employee.setLastName("Sundaysky");
        employee.setFirstName("Chrisopher");
        employee.setEmail("admin@admin.pl");
        employee.setRole(Role.DEVELOPER);
        employee.setTeams(new HashSet<Team>());

        //when
        EmployeeCommand employeeCommand = converter.convert(employee);

        //then
        assertEquals(employeeCommand.getPesel(),employee.getPesel());
        assertEquals(employeeCommand.getId(),employee.getId());
//        assertEquals(employeeCommand.getRole(),employee.getRole().toString());
    }

}