package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.TeamCommand;
import com.ksundaysky.agileboard.domain.Team;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TeamToTeamCommandTest {

    TeamToTeamCommand converter;
    @Before
    public void setUp() throws Exception {
        converter = new TeamToTeamCommand();
    }

    @Test
    public void testNullObject()  {
        assertNull(converter.convert(null));
    }

    @Test
    public void testEmptyObject() throws Exception {
        assertNotNull(converter.convert(new Team()));
    }

    @Test
    public void convert() throws Exception {

        //given

        Team team = new Team();
        team.setName("Pumba");
        team.setId(1l);

        //when

        TeamCommand teamCommand = converter.convert(team);

        //then

        assertEquals(team.getId(),teamCommand.getId());
        assertEquals(team.getName(),teamCommand.getName());
    }

}