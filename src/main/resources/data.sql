INSERT INTO team (name,description) VALUES ('FastAndFurious','Senior Java Team + AWS Cloud + coffee lovers');
INSERT INTO team (name,description) VALUES ('CrazyFrogs','Scala Team + Big Data + NoSQL');
INSERT INTO team (name,description) VALUES ('Unicorns','C++ enthusiasts + C# sucks!');
INSERT INTO team (name,description) VALUES ('BeerLovers','Spring Framework is everything!');
INSERT INTO team (name,description) VALUES ('DataFreaks','Hibernate (and ORM) sucks only pure SQL queries');



INSERT INTO project (name,description) VALUES ('BankingSystemHSBC', 'Application to connect HSBC places all over the world');
INSERT INTO project (name,description) VALUES ('OnlineShopDecathlon', 'Online shop with advanced customer support with AI');
INSERT INTO project (name,description) VALUES ('RiskManagementUBS', 'Risk Management Tool with EA(extended reality) feature');


INSERT INTO sprint(id,start_of_sprint,end_of_sprint,name,project_id,is_active) VALUES (1,'2018-01-01','2018-01-31','SPRINT 1',1,true );
INSERT INTO sprint(id,start_of_sprint,end_of_sprint,name,project_id,is_active) VALUES (2,'2018-02-01','2018-02-28','SPRINT 2',1,FALSE );
INSERT INTO sprint(id,start_of_sprint,end_of_sprint,name,project_id,is_active) VALUES (3,'2018-03-01','2018-03-31','SPRINT 3',1,FALSE );


INSERT INTO employee(id) VALUES (1);
INSERT INTO employee(id) VALUES (2);
INSERT INTO employee(id) VALUES (3);
INSERT INTO employee(id) VALUES (4);
INSERT INTO employee(id) VALUES (5);
INSERT INTO employee(id) VALUES (6);
INSERT INTO employee(id) VALUES (7);
INSERT INTO employee(id) VALUES (8);
INSERT INTO employee(id) VALUES (9);
INSERT INTO employee(id) VALUES (10);
INSERT INTO employee(id) VALUES (11);
INSERT INTO employee(id) VALUES (12);
INSERT INTO employee(id) VALUES (13);
INSERT INTO employee(id) VALUES (14);
INSERT INTO employee(id) VALUES (15);
INSERT INTO employee(id) VALUES (16);
INSERT INTO employee(id) VALUES (17);
INSERT INTO employee(id) VALUES (18);
INSERT INTO employee(id) VALUES (19);
INSERT INTO employee(id) VALUES (20);
INSERT INTO employee(id) VALUES (21);
INSERT INTO employee(id) VALUES (22);

INSERT INTO task(name, is_assigned) VALUES ('Logging panel',FALSE);
INSERT INTO task(name, is_assigned) VALUES ('Valid login form',FALSE);
INSERT INTO task(name, is_assigned) VALUES ('Frontend - form logging in',FALSE);
INSERT INTO task(name, is_assigned) VALUES ('REST controller for adding new customer',FALSE);
--
INSERT INTO task(id,is_assigned,name,priority,status,project_id,employee_id,date_when_done,sprint_id) VALUES (5,TRUE ,'AWS connection',1,3,1,3,'2018-01-04',1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,employee_id,date_when_done,sprint_id) VALUES (6,TRUE,'Bootstrap navbar',2,3,1,3,'2018-01-20',1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,employee_id,date_when_done,sprint_id) VALUES (7,TRUE,'Dijkstra algorithm',1,3,1,4,'2018-01-16',1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,employee_id,date_when_done,sprint_id) VALUES (8,TRUE,'BLIK integration',0,3,1,4,'2018-01-10',1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,employee_id,date_when_done,sprint_id) VALUES (9,TRUE,'VISA integration',1,3,1,5,'2018-01-25',1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,employee_id,date_when_done,sprint_id) VALUES (10,TRUE,'task to do something',2,3,1,5,'2018-01-13',1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,sprint_id) VALUES (11,FALSE,'refactor some',2,0,1,1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,sprint_id) VALUES (12,FALSE,'css sucks fix it',1,0,1,1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,employee_id,sprint_id) VALUES (13,TRUE,'find customers by name',1,1,1,2,1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,employee_id,sprint_id) VALUES (14,TRUE,'Presentaton for client',1,1,1,2,1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,employee_id,sprint_id) VALUES (15,TRUE,'Test cases for controller',2,1,1,5,1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,employee_id,sprint_id) VALUES (16,TRUE,'Sprint delete feture',2,2,1,4,1);
INSERT INTO task(id,is_assigned,name,priority,status,project_id,employee_id,sprint_id) VALUES (17,TRUE,'Sprint update feture',2,2,1,6,1);





-- INSERT INTO project (name) VALUES ('Mexican');
-- INSERT INTO team (name) VALUES ('bajorex');
-- INSERT INTO team (name) VALUES ('delta_przyspieszenie');
-- INSERT INTO team (name) VALUES ('unix_unicorns');
-- INSERT INTO task (name) VALUES ('logging in');
-- INSERT INTO task (name) VALUES ('css fix');
-- INSERT INTO task (name) VALUES ('db test');

