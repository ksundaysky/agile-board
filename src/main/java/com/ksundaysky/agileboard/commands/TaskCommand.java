package com.ksundaysky.agileboard.commands;

import com.ksundaysky.agileboard.domain.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TaskCommand {
    private Long id;
    private String name;

    private Boolean isAssigned;

    private String dateWhenDone;

    private String priority;
    private Status status;

    private Long projectId;
    private Project project;
    private Employee employee;
    private Sprint sprint;
}
