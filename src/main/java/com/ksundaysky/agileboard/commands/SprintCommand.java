package com.ksundaysky.agileboard.commands;

import com.ksundaysky.agileboard.domain.Project;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
public class SprintCommand {

    private Long id;

    private String name;

    private Boolean isActive;

    private String startOfSprint;

    private String endOfSprint;

    private Project project;

    private Long projectId;

    private Set<TaskCommand> tasks = new HashSet<>();
}
