package com.ksundaysky.agileboard.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeCommand {

    private Long id;
    private String firstName;
    private String lastName;
    private String pesel;
    private String email;
    private String role;
    private Set<ProjectCommand> projects = new HashSet<>();
    private Set<TeamCommand> teams = new HashSet<>();
    private String password;
}
