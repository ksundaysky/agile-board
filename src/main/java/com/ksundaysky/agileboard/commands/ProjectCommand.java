package com.ksundaysky.agileboard.commands;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCommand {
    private Long id;
    private String name;
    private String description;
    private Set<TeamCommand> teams = new HashSet<>();
    private Set<TaskCommand> tasks = new HashSet<>();
}
