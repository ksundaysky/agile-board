package com.ksundaysky.agileboard.commands;


import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//@Setter
//@Getter
//@NoArgsConstructor
@Data
public class TeamCommand {



    private Long id;
    private String name;
    private int howManyMembers;
    private int howManyProjects;
    private String description;
}
