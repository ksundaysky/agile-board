package com.ksundaysky.agileboard.Services;

import com.google.common.collect.Sets;
import com.ksundaysky.agileboard.domain.Task;
import com.ksundaysky.agileboard.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    TaskRepository taskRepository;

    @Override
    public Set<Task> findAll() {
        return Sets.newHashSet(taskRepository.findAll());
    }

    @Override
    public Task findById(Long id) {
        return taskRepository.findById(id).get();
    }

    @Override
    public Task save(Task task) {
        return taskRepository.save(task);
    }

    @Override
    public Task findByName(String name) {

        if(taskRepository.findByName(name).isPresent()){
            return taskRepository.findByName(name).get();
        }else{
            return null;
        }
    }
}
