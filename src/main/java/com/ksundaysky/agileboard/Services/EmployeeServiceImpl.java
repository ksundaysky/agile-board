package com.ksundaysky.agileboard.Services;

import com.ksundaysky.agileboard.commands.EmployeeCommand;
import com.ksundaysky.agileboard.converters.EmployeeCommandToEmployee;
import com.ksundaysky.agileboard.domain.Employee;
import com.ksundaysky.agileboard.domain.Role;
import com.ksundaysky.agileboard.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class
EmployeeServiceImpl implements EmployeeService {



    private EmployeeRepository employeeRepository;

    private EmployeeCommandToEmployee converter;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
        converter = new EmployeeCommandToEmployee();
    }

    @Override
    public Set<Employee> findAll() {
        Set<Employee> employees = new HashSet<>();



        this.employeeRepository.findAll().iterator().forEachRemaining(employees::add);
        return employees;
    }


    @Override
    @Transactional
    public Employee saveEmployee(EmployeeCommand command){

        Employee employee = converter.convert(command);

        employee.setPassword(bCryptPasswordEncoder.encode(employee.getPassword()));

        employeeRepository.save(employee);

        return employee;
    }

    @Override
    public Employee findById(long id) {
        Employee employee = employeeRepository.findById(id).get();

        return employee;
    }

    @Override
    public Employee findEmployeeByEmail(String email) {

        return employeeRepository.findByEmail(email);
    }
}
