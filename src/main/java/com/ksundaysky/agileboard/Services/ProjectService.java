package com.ksundaysky.agileboard.Services;

import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.domain.Project;

import java.util.Set;

public interface ProjectService {

    Set<Project> findAll();
    Project findById(Long id);
    Project findByName(String name);
    Project saveProject(ProjectCommand project);
}
