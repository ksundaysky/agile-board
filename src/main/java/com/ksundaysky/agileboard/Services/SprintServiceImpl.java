package com.ksundaysky.agileboard.Services;

import com.google.common.collect.Sets;
import com.ksundaysky.agileboard.domain.Sprint;
import com.ksundaysky.agileboard.repositories.SprintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class SprintServiceImpl implements SprintService {

    @Autowired
    SprintRepository sprintRepository;

    @Override
    public Sprint save(Sprint sprint) {
        return sprintRepository.save(sprint);
    }

    @Override
    public Set<Sprint> findAll() {
        return  Sets.newHashSet(sprintRepository.findAll());
    }

    @Override
    public Sprint findById(Long id) {
        return sprintRepository.findById(id).get();
    }

    @Override
    public Sprint findByName(String name) {
        return sprintRepository.findByName(name);
    }
}
