package com.ksundaysky.agileboard.Services;

import com.google.common.collect.Sets;
import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.converters.ProjectCommandToProject;
import com.ksundaysky.agileboard.domain.Project;
import com.ksundaysky.agileboard.repositories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class ProjectServiceImpl implements ProjectService {


    @Autowired
    ProjectRepository projectRepository;

    ProjectCommandToProject converter = new ProjectCommandToProject();


    @Override
    public Set<Project> findAll() {
        return Sets.newHashSet(projectRepository.findAll()) ;
    }

    @Override
    public Project findById(Long id) {
        return projectRepository.findById(id).get();
    }

    @Override
    public Project findByName(String name) {
        if(projectRepository.findByName(name).isPresent()){
            return projectRepository.findByName(name).get();
        }else{
            return null;
        }
    }

    @Override
    public Project saveProject(ProjectCommand projectCommand) {

        Project project = converter.convert(projectCommand);
        projectRepository.save(project);
        return project;
    }
}
