package com.ksundaysky.agileboard.Services;

import com.ksundaysky.agileboard.domain.Task;

import java.util.Set;

public interface TaskService {

    Set<Task> findAll();
    Task findById(Long id);
    Task save(Task task);
    Task findByName(String name);

}
