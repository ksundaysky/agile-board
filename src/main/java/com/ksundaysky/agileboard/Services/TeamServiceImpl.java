package com.ksundaysky.agileboard.Services;


import com.google.common.collect.Sets;
import com.ksundaysky.agileboard.commands.TeamCommand;
import com.ksundaysky.agileboard.converters.TeamCommandToTeam;
import com.ksundaysky.agileboard.domain.Team;
import com.ksundaysky.agileboard.repositories.TeamRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.HashSet;
import java.util.Set;

@Service
public class TeamServiceImpl implements TeamService {

    TeamRepository teamRepository;
    private TeamCommandToTeam converter;

    public TeamServiceImpl(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
        this.converter = new TeamCommandToTeam();
    }

    @Override
    public Set<Team> findAll() {

        return Sets.newHashSet(teamRepository.findAll());
    }

    @Override
    public Team findById(Long id) {
        return teamRepository.findById(id).get();
    }

    @Override
    public Team findByName(String name) {

       if(teamRepository.findByName(name).isPresent()){
           return teamRepository.findByName(name).get();
       }else{
           return null;
       }
    }


    @Override
    @Transactional
    public Team saveTeam(TeamCommand command){

        Team team = converter.convert(command);

        System.out.println("team name = "+team.getName()+" id = "+team.getId()+" desc = "+team.getDescription()+" empl="+team.getEmployees().size());
        teamRepository.save(team);

        return team;
    }
}
