package com.ksundaysky.agileboard.Services;

import com.ksundaysky.agileboard.commands.EmployeeCommand;
import com.ksundaysky.agileboard.domain.Employee;
import com.ksundaysky.agileboard.repositories.EmployeeRepository;

import java.util.Set;

public interface EmployeeService{
 Set<Employee> findAll();
 Employee saveEmployee(EmployeeCommand command);
 Employee findById(long id);
 Employee findEmployeeByEmail(String email);

}
