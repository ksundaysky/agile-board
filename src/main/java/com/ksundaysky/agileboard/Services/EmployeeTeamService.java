package com.ksundaysky.agileboard.Services;

import java.util.List;

public interface EmployeeTeamService {

    List<Long> findByTeamId(Long teamId);
}
