package com.ksundaysky.agileboard.Services;

import com.ksundaysky.agileboard.commands.TeamCommand;
import com.ksundaysky.agileboard.domain.Team;

import java.util.Set;

public interface TeamService {
    Set<Team> findAll();
    Team findById(Long id);
    Team findByName(String name);
    Team saveTeam(TeamCommand team);

}
