package com.ksundaysky.agileboard.Services;

import com.ksundaysky.agileboard.domain.Sprint;

import java.util.Set;

public interface SprintService {

    Sprint save(Sprint sprint);
    Set<Sprint> findAll();
    Sprint findById(Long id);
    Sprint findByName(String name);
}
