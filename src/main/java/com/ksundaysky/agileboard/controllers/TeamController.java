package com.ksundaysky.agileboard.controllers;


import com.ksundaysky.agileboard.Services.EmployeeService;
import com.ksundaysky.agileboard.Services.EmployeeTeamService;
import com.ksundaysky.agileboard.Services.TeamService;
import com.ksundaysky.agileboard.commands.EmployeeCommand;
import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.commands.TeamCommand;
import com.ksundaysky.agileboard.converters.EmployeeToEmployeeCommand;
import com.ksundaysky.agileboard.converters.ProjectToProjectCommand;
import com.ksundaysky.agileboard.converters.TeamToTeamCommand;
import com.ksundaysky.agileboard.domain.Employee;
import com.ksundaysky.agileboard.domain.Project;
import com.ksundaysky.agileboard.domain.Team;
import com.ksundaysky.agileboard.repositories.TeamRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.*;

@Controller
public class TeamController {


    private TeamService teamService;
    private EmployeeService employeeService;

    private TeamToTeamCommand converter;
    private EmployeeToEmployeeCommand converterEmployee;
    private ProjectToProjectCommand converterProject;

    public TeamController(TeamService teamService, EmployeeService employeeService) {
        this.teamService = teamService;
        this.employeeService = employeeService;
        this.converterEmployee = new EmployeeToEmployeeCommand();
        this.converter = new TeamToTeamCommand();
        this.converterProject = new ProjectToProjectCommand();
    }

    @RequestMapping("/team/list")
    public String getTeamList(Model model){

        Set<TeamCommand> teams = new HashSet<>();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        GrantedAuthority roles = (GrantedAuthority) auth.getAuthorities().toArray()[0];
        String role = roles.getAuthority();



        if(("ADMIN").equals(role))
            teamService.findAll().forEach(t -> teams.add(converter.convert(t)));
        if(("DEVELOPER").equals(role)){
            Employee employee = employeeService.findEmployeeByEmail(auth.getName());
            employee.getTeams().forEach(t -> teams.add(converter.convert(t)));
        }
        model.addAttribute("teams",teams);

        return"/team/list";
    }


    @RequestMapping("/team/{id}")
    public String getTeamId(Model model, @PathVariable Long id){

        Team team = teamService.findById(id);
        List<EmployeeCommand> employees = new ArrayList<>();
        Set<ProjectCommand> projects = new HashSet<>();

        team.getEmployees().forEach(e->employees.add(converterEmployee.convert(e)));
        team.getProjects().forEach(p->projects.add(converterProject.convert(p)));

        model.addAttribute("teams", Collections.singletonList(converter.convert(team)));
        model.addAttribute("employees",employees);
        model.addAttribute("projects",projects);

        return"/team/id";
    }


    @RequestMapping(value = "/team/create", method = RequestMethod.GET)
    public String getAddPage(Model model){

        if(!model.containsAttribute("team")) {
            model.addAttribute("team", new TeamCommand());
        }
        return "/team/add";
    }

    @PostMapping("/team/create")
    public String save(@Valid @ModelAttribute("team") TeamCommand team, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes){

        Team ifTeamExist = teamService.findByName(team.getName());

        if(ifTeamExist!=null){
            bindingResult.rejectValue("name","error.team","this name is already in use");
        }
        if(bindingResult.hasErrors())
        {
            System.out.println(bindingResult.getAllErrors());
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.team",bindingResult);
            redirectAttributes.addFlashAttribute("team",team);
//            model.addAttribute("employee",command);
            return "redirect:/team/create";
        }
        else {

            Team team1 = teamService.saveTeam(team);

            return "redirect:/team/" + team1.getId();
        }
    }


    @RequestMapping("/team/{id}/employee/add")
    public String addNewTeamMember(Model model, @PathVariable Long id){

        Set<Employee> employees = employeeService.findAll();
        employees.removeAll(teamService.findById(id).getEmployees());

        model.addAttribute("employees",employees);
        model.addAttribute("teamId",id);
        return"/team/employee/new";
    }

    @RequestMapping("/team/{teamId}/employee/add/{employeeId}")
    public String addNewTeamMember(Model model, @PathVariable Long teamId,@PathVariable Long employeeId){

        Employee employee = employeeService.findById(employeeId);
        employee.getTeams().add(teamService.findById(teamId));
        employeeService.saveEmployee(converterEmployee.convert(employee));

        return"redirect:/team/"+teamId;
    }
}
