package com.ksundaysky.agileboard.controllers;

import com.google.common.collect.Sets;
import com.ksundaysky.agileboard.Services.EmployeeService;
import com.ksundaysky.agileboard.Services.ProjectService;
import com.ksundaysky.agileboard.Services.TaskService;
import com.ksundaysky.agileboard.commands.TaskCommand;
import com.ksundaysky.agileboard.converters.TaskCommandToTask;
import com.ksundaysky.agileboard.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    ProjectService projectService;

    @Autowired
    EmployeeService employeeService;

    TaskCommandToTask taskConverter = new TaskCommandToTask();



    @RequestMapping("/task/{taskId}/removeassigne")
    public String removeAssigneFromTask(@PathVariable Long taskId){

        Task task = taskService.findById(taskId);
        task.setIsAssigned(false);
        task.setEmployee(null);
        taskService.save(task);
        return"redirect:/project/"+task.getProject().getId()+"/sprints";
    }

    @RequestMapping("/task/{taskId}/assign")
    public String assignThisTask(Model model,@PathVariable Long taskId){

        Task task = taskService.findById(taskId);

        Set<Employee> employees = task.getProject().getEmployees();
        model.addAttribute("tasks", Sets.newHashSet(task));
        model.addAttribute("employees", employees);

        return"/project/task/assign";
    }

    @RequestMapping("/task/{taskId}/changeassign")
    public String changeassignThisTask(Model model,@PathVariable Long taskId){

        Task task = taskService.findById(taskId);

        Set<Employee> employees = task.getProject().getEmployees();
        employees.remove(task.getEmployee());
        model.addAttribute("tasks", Sets.newHashSet(task));
        model.addAttribute("employees", employees);

        return"/project/task/changeassign";
    }

    @RequestMapping("/project/{projectId}/task/create")
    public String addTaskToProject(Model model, @PathVariable Long projectId){

        TaskCommand task = new TaskCommand();
        Project project = projectService.findById(projectId);
        System.out.println(project.getName());
        task.setProject(project);
        model.addAttribute("task",task);
        model.addAttribute("projectId",projectId);

        return"/project/task/add";
    }

    @PostMapping("/project/task/create")
    public String addingTask(@Valid @ModelAttribute TaskCommand task, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes){

        System.out.println("projekt = "+task.getProjectId());

        Task ifTaskExist = taskService.findByName(task.getName());

        if(ifTaskExist!=null){
            bindingResult.rejectValue("name","error.task","this name is already in use");
        }
        if(bindingResult.hasErrors())
        {
            System.out.println(bindingResult.getAllErrors());
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.team",bindingResult);
            redirectAttributes.addFlashAttribute("task",task);
//            model.addAttribute("employee",command);
            return "redirect:/project/"+task.getProjectId()+"/task/create";
        }
        else {

//            Team team1 = teamService.saveTeam(team);
           task.setProject(projectService.findById(task.getProjectId()));
           task.setStatus(Status.TODO);
           taskService.save(taskConverter.convert(task));

            return "redirect:/project/" + task.getProjectId()+"/backlog";
        }

//        return"redirect : /index";
    }


    @RequestMapping("/employee/{employeeId}/task/{taskId}/assign")
    public String assignTaskToEmployee(Model model, @PathVariable Long employeeId, @PathVariable Long taskId){

        Task task = taskService.findById(taskId);
        Employee employee = employeeService.findById(employeeId);
        task.setEmployee(employee);
        task.setIsAssigned(true);
        taskService.save(task);

        return "redirect:/project/"+task.getProject().getId()+"/sprints";
    }

//    @RequestMapping("/task/{taskId}/assign")
//    public String assignTask(Model model, @PathVariable Long taskId){
//
//        Task task = taskService.findById(taskId);
//        Project project = task.getProject();
//
//        Set<Employee> employees = new HashSet<>(project.getEmployees());
//
//        model.addAttribute("tasks",task);
//        model.addAttribute("employees",employees);
//
//        return "/task/assign";
//    }
}
