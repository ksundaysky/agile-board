package com.ksundaysky.agileboard.controllers;


import com.ksundaysky.agileboard.Services.*;
import com.ksundaysky.agileboard.commands.SprintCommand;
import com.ksundaysky.agileboard.commands.TaskCommand;
import com.ksundaysky.agileboard.converters.EmployeeToEmployeeCommand;
import com.ksundaysky.agileboard.converters.ProjectToProjectCommand;
import com.ksundaysky.agileboard.converters.SprintCommandToSprint;
import com.ksundaysky.agileboard.domain.Project;
import com.ksundaysky.agileboard.domain.Sprint;
import com.ksundaysky.agileboard.domain.Status;
import com.ksundaysky.agileboard.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class SprintController {

    @Autowired
    ProjectService projectService;

    @Autowired
    TeamService teamService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    TaskService taskService;

    @Autowired
    SprintService sprintService;

    SprintCommandToSprint sprintConverter = new SprintCommandToSprint();

    ProjectToProjectCommand converter = new ProjectToProjectCommand();

    EmployeeToEmployeeCommand converterEmployee = new EmployeeToEmployeeCommand();

    @RequestMapping("/project/{projectId}/task/{taskId}/removefromsprint")
    public String removeTaskFromSprintAndSetToDefault(Model model, @PathVariable Long projectId,@PathVariable Long taskId){

        Task t = taskService.findById(taskId);
        t.setSprint(null);
        t.setStatus(Status.TODO);
        t.setDateWhenDone(null);
        t.setIsAssigned(false);
        t.setEmployee(null);

        taskService.save(t);
        return"redirect:/project/"+projectId+"/sprints";
    }

    @RequestMapping("/project/{projectId}/sprint/{sprintId}/close")
    public String closeSprint(@PathVariable Long projectId, @PathVariable Long sprintId){

        Sprint sprint = sprintService.findById(sprintId);
        sprint.setIsActive(false);
        sprintService.save(sprint);

        return"redirect:/project/"+projectId+"/sprints";
    }

    @RequestMapping("/project/{projectId}/sprint/{sprintId}")
    public String getSprint(Model model,@PathVariable Long projectId, @PathVariable Long sprintId){


        if(sprintService.findById(sprintId).getIsActive())
            return"redirect:/project/"+projectId+"/sprints";

        Project project = projectService.findById(projectId);


        Set<Sprint> sprintNames = new HashSet<>();



        Set<Sprint> sprint = new HashSet<>();//new Sprint();// = null;
        for (Sprint s : project.getSprints()) {

            sprintNames.add(s);
            if(s.getId().equals(sprintId))
            {
                sprint.add(s);
            }
        }


        Set<Task> tasks = new HashSet<>();

        if(sprint.iterator().hasNext()) {

            Optional<Set<Task>> taskOptional = Optional.of(sprint.iterator().next().getTasks());

            if(taskOptional.isPresent()){
                sprint.iterator().next().getTasks().forEach(t->tasks.add(t));
            }


        }

        Set<Task> todoTasks = tasks.stream()
                .filter(t-> (Status.TODO).equals(t.getStatus()))
                .collect(Collectors.toSet());
        Set<Task> inProgressTasks= tasks.stream()
                .filter(t-> (Status.IN_PROGRESS).equals(t.getStatus()))
                .collect(Collectors.toSet());
        Set<Task> inTestsTasks= tasks.stream()
                .filter(t-> (Status.IN_TEST).equals(t.getStatus()))
                .collect(Collectors.toSet());
        Set<Task> doneTasks= tasks.stream()
                .filter(t-> (Status.DONE).equals(t.getStatus()))
                .collect(Collectors.toSet());



        model.addAttribute("projects",project);
        model.addAttribute("sprintNames",sprintNames);
        model.addAttribute("sprints",sprint);
        model.addAttribute("todoTasks",todoTasks);
        model.addAttribute("inProgressTasks",inProgressTasks);
        model.addAttribute("inTestsTasks",inTestsTasks);
        model.addAttribute("doneTasks",doneTasks);
        model.addAttribute("name",new Sprint());
        model.addAttribute("mainSprint", sprint);



        return"/project/sprint/id";

    }

    @RequestMapping("/project/{projectId}/sprints/create")
    public String addSprint(Model model,@PathVariable Long projectId){

        SprintCommand sprintCommand = new SprintCommand();

        model.addAttribute("sprint",sprintCommand);
        model.addAttribute("projectId",projectId);



        return"/project/sprint/add";
    }

    @PostMapping("/project/sprint/create")
    public String addingSprint(@Valid @ModelAttribute SprintCommand sprint, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes){

        Task ifTaskExist = taskService.findByName(sprint.getName());

        if(ifTaskExist!=null){
            bindingResult.rejectValue("name","error.sprint","this name is already in use");
        }
        if(bindingResult.hasErrors())
        {
            System.out.println(bindingResult.getAllErrors());
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.team",bindingResult);
            redirectAttributes.addFlashAttribute("sprint",sprint);
//            model.addAttribute("employee",command);
            return "redirect:/project/"+sprint.getProjectId()+"/task/create";
        }
        else {

//            Team team1 = teamService.saveTeam(team);
//            sprint.setProject(projectService.findById(sprint.getProjectId()));
            sprint.setIsActive(true);
            sprint.setProject(projectService.findById(sprint.getProjectId()));
//            System.out.println(sprint.getProjectId());
            sprintService.save(sprintConverter.convert(sprint));
            return "redirect:/project/" + sprint.getProjectId()+"/sprints";
        }

//        return"redirect : /index";
    }



    @PostMapping("/sprint/id/post")
    public String gettingSprint(@Valid @ModelAttribute SprintCommand sprint, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes){

        Task ifTaskExist = taskService.findByName(sprint.getName());

        String name = sprint.getName();

        Sprint sprint1 = sprintService.findByName(name);

        return "redirect:/project/"+sprint1.getProject().getId()+"/sprint/"+sprint1.getId();

//        if(ifTaskExist!=null){
//            bindingResult.rejectValue("name","error.sprint","this name is already in use");
//        }
//        if(bindingResult.hasErrors())
//        {
//            System.out.println(bindingResult.getAllErrors());
//            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.team",bindingResult);
//            redirectAttributes.addFlashAttribute("sprint",sprint);
////            model.addAttribute("employee",command);
//            return "redirect:/project/"+sprint.getProjectId()+"/task/create";
//        }
//        else {
//
////            Team team1 = teamService.saveTeam(team);
////            sprint.setProject(projectService.findById(sprint.getProjectId()));
//            sprint.setIsActive(true);
//            sprint.setProject(projectService.findById(sprint.getProjectId()));
////            System.out.println(sprint.getProjectId());
//            sprintService.save(sprintConverter.convert(sprint));
//            return "redirect:/project/" + sprint.getProjectId()+"/sprints";
//        }

//        return"redirect : /index";
    }

}

