package com.ksundaysky.agileboard.controllers;


import com.ksundaysky.agileboard.Services.EmployeeService;
import com.ksundaysky.agileboard.Services.TeamService;
import com.ksundaysky.agileboard.commands.EmployeeCommand;
import com.ksundaysky.agileboard.converters.EmployeeToEmployeeCommand;
import com.ksundaysky.agileboard.domain.Employee;
import com.ksundaysky.agileboard.domain.Team;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.jws.WebParam;
import javax.validation.Valid;
import java.util.Collections;

@Controller
public class EmployeeController {


    private EmployeeService employeeService;
    private TeamService teamService;

    private EmployeeToEmployeeCommand converter = new EmployeeToEmployeeCommand();

    public EmployeeController(EmployeeService employeeService, TeamService teamService) {
        this.employeeService = employeeService;
        this.teamService = teamService;
    }

    @RequestMapping("/employee/list")
    public String getEmployees(Model model){
        model.addAttribute("employees",employeeService.findAll());
        return "/employee/list";
    }


    @RequestMapping(value = "/employee/create", method = RequestMethod.GET)
    public String getAddPage(Model model){

        if(!model.containsAttribute("employee")) {
            model.addAttribute("employee", new EmployeeCommand());
        }
        return "/employee/add";
    }


    @PostMapping("/employee/create")
    public String save(@Valid @ModelAttribute("employee") EmployeeCommand employee, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes){

        Employee ifEmployeeExist = employeeService.findEmployeeByEmail(employee.getEmail());

        if(ifEmployeeExist!=null){
            bindingResult.rejectValue("email","error.employee","this email is already in use");
        }
        if(bindingResult.hasErrors())
        {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.employee",bindingResult);
            redirectAttributes.addFlashAttribute("employee",employee);

            return "redirect:/employee/create";
        }
        else {

            Employee employeed = employeeService.saveEmployee(employee);

            return "redirect:/employee/" + employeed.getId();
        }
    }

    @RequestMapping("/employee/{id}")
    public String showEmployee(Model model,@PathVariable long id) {

        Employee employee = employeeService.findById(id);

//        employee.getTeams().forEach(e -> System.out.println(e.getName()));

        model.addAttribute("employee",employee);
        return "/employee/id";
    }


    @RequestMapping("/employee/{employeeId}/team/{teamId}/remove")
    public String removeEmployeeFromTeam(Model model, @PathVariable(value = "employeeId") Long employeeId,@PathVariable(value = "teamId") Long teamId){

      Employee employee = employeeService.findById(employeeId);
      Team team = teamService.findById(teamId);
      employee.getTeams().removeIf(t -> t.getId().equals(teamId));
      employee.getProjects().removeIf(p->team.getProjects().contains(p) & Collections.disjoint(p.getTeams(),employee.getTeams()));
      employeeService.saveEmployee(converter.convert(employee));

        return"redirect:/team/"+teamId;
    }



}
