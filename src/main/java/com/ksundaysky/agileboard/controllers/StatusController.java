package com.ksundaysky.agileboard.controllers;

import com.ksundaysky.agileboard.Services.TaskService;
import com.ksundaysky.agileboard.domain.Status;
import com.ksundaysky.agileboard.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;


@Controller
public class StatusController {

    @Autowired
    TaskService taskService;

    @RequestMapping("/task/{taskId}/changestatus/todo")
    public String changeTaskStatusTODO(@PathVariable Long taskId,HttpServletRequest request){

        String back = request.getHeader("Referer");

        Task task  = taskService.findById(taskId);
        task.setStatus(Status.TODO);
        taskService.save(task);
        return "redirect:"+back;
    }

    @RequestMapping("/task/{taskId}/changestatus/inprogress")
    public String changeTaskStatusINPROGRESS(@PathVariable Long taskId,HttpServletRequest request){

        String back = request.getHeader("Referer");
        Task task  = taskService.findById(taskId);
        task.setStatus(Status.IN_PROGRESS);
        taskService.save(task);
        return "redirect:"+back;
    }

    @RequestMapping("/task/{taskId}/changestatus/intests")
    public String changeTaskStatusINTESTS(@PathVariable Long taskId,HttpServletRequest request){

        String back = request.getHeader("Referer");

        Task task  = taskService.findById(taskId);
        task.setStatus(Status.IN_TEST);
        task.setDateWhenDone(null);
        taskService.save(task);
        return "redirect:"+back;
    }

    @RequestMapping("/task/{taskId}/changestatus/done")
    public String changeTaskStatusDONE(@PathVariable Long taskId,HttpServletRequest request){

        String back = request.getHeader("Referer");

        Task task  = taskService.findById(taskId);
        task.setStatus(Status.DONE);
        task.setDateWhenDone(new Date(System.currentTimeMillis()));
        taskService.save(task);
        return "redirect:"+back;
    }
}
