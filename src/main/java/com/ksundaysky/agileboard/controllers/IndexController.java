package com.ksundaysky.agileboard.controllers;


import com.ksundaysky.agileboard.Services.EmployeeService;
import com.ksundaysky.agileboard.commands.EmployeeCommand;
import com.ksundaysky.agileboard.domain.Employee;
import com.ksundaysky.agileboard.domain.Status;
import com.ksundaysky.agileboard.domain.Task;
import com.ksundaysky.agileboard.repositories.TaskRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static com.ksundaysky.agileboard.domain.Status.TODO;
import static java.lang.Math.round;

@Slf4j
@Controller
public class IndexController {

    EmployeeService employeeService;
    @Autowired
    TaskRepository taskRepository;

    public IndexController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @RequestMapping("/login")
    public String getIndexPage(Model model) throws ParseException {
        log.debug("get index page");


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(taskRepository.findById(10L).get().getDateWhenDone());
        System.out.println(cal.get(Calendar.YEAR));
        System.out.println(cal.get(Calendar.MONTH));
        System.out.println(cal.get(Calendar.DAY_OF_MONTH));



        System.out.println("dupa");
//        model.addAttribute("employees", employeeService.findAll());
        return "test";
    }

//    @RequestMapping("/loging")
//    public String getLogin(Model model){
//        return "login";
//    }

//    @RequestMapping("/home")
//    public String getList(Model model){
//        log.debug("get index page");
//
////        model.addAttribute("employees", employeeService.findAll());
//        return "index";
//    }







    @RequestMapping(value={"/","/home"}, method = RequestMethod.GET)
    public ModelAndView home(){
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Employee employee = employeeService.findEmployeeByEmail(auth.getName());
        System.out.println("AUTHORITIES = "+auth.getAuthorities().toArray()[0]);

        if(employee == null)
            modelAndView.setViewName("/test");
        else {
            double total=0.0;
            double todo=0.0;
            double inprogress=0.0;
            double intests=0.0;
            double done=0.0;



            Set<Task> tasks = new HashSet<>();

            GrantedAuthority roles = (GrantedAuthority) auth.getAuthorities().toArray()[0];
            String role = roles.getAuthority();
            if(("ADMIN").equals(role))
                taskRepository.findAll().forEach(task ->
                {
                    if(task.getSprint()!=null && task.getSprint().getIsActive()){
                        tasks.add(task);
                    }
                });
            if(("DEVELOPER").equals(role))
                employee.getTasks().forEach(task ->
                {
                    if(task.getSprint().getIsActive()){
                        tasks.add(task);
                    }
                });
            if(("SCRUM_MASTER").equals(role))
                employee.getProjects().forEach(p -> p.getTasks().forEach(task ->
                {
                    if(task.getSprint()!=null && task.getSprint().getIsActive()){
                        tasks.add(task);
                    }
                }));

            if(("PRODUCT_OWNER").equals(role))
                employee.getProjects().forEach(p -> p.getTasks().forEach(task ->
                {
                    if(task.getSprint()!=null && task.getSprint().getIsActive()){
                        tasks.add(task);
                    }
                }));

            for(Task t : tasks){

                total++;
                switch(t.getStatus()){
                    case TODO:
                        todo++;
                        break;
                    case IN_PROGRESS:
                        inprogress++;
                        break;
                    case IN_TEST:
                        intests++;
                        break;
                    case DONE:
                        done++;
                        break;
                }



            }

            double perc_todo ;
            double perc_inprogres ;
            double perc_intests ;
            double perc_done ;
            if(total ==0) {

                perc_todo = 0;
                perc_inprogres = 0;
                perc_intests = 0;
                perc_done = 0;
            }
            else{
                 perc_todo = (todo/total)*100;
                 perc_inprogres = (inprogress/total)*100;
                 perc_intests = (intests/total)*100;
                 perc_done = (done/total)*100;
            }


            System.out.println(employee.getRole());
//            modelAndView.addObject("userName", "Witaj " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
//            modelAndView.addObject("adminMessage", "Użytkownik " +  roleService.getRoleById(user.getRole_id()).getRole());
//            loggedInUser = user;

            DecimalFormat df = new DecimalFormat("#.##");
            modelAndView.addObject("total",total);
            modelAndView.addObject("todo",todo);
            modelAndView.addObject("inprogres",inprogress);
            modelAndView.addObject("intests",intests);
            modelAndView.addObject("done",done);
            modelAndView.addObject("percTodo",new BigDecimal(perc_todo).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
            modelAndView.addObject("percInProgres",new BigDecimal(perc_inprogres).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
            modelAndView.addObject("percInTests",new BigDecimal(perc_intests).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
            modelAndView.addObject("percDone",new BigDecimal(perc_done).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());

            System.out.println("todo = "+todo);
            System.out.println("todo perc  = "+perc_todo);


            modelAndView.setViewName("/index");
        }

        return modelAndView;
    }

}
