package com.ksundaysky.agileboard.controllers;


import com.google.common.collect.Sets;
import com.ksundaysky.agileboard.Services.EmployeeService;
import com.ksundaysky.agileboard.Services.ProjectService;
import com.ksundaysky.agileboard.Services.TeamService;
import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.commands.TaskCommand;
import com.ksundaysky.agileboard.converters.EmployeeToEmployeeCommand;
import com.ksundaysky.agileboard.converters.ProjectToProjectCommand;
import com.ksundaysky.agileboard.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.swing.table.TableModel;
import javax.validation.Valid;
import javax.validation.constraints.Null;
import java.time.temporal.Temporal;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @Autowired
    TeamService teamService;

    @Autowired
    EmployeeService employeeService;

    ProjectToProjectCommand converter = new ProjectToProjectCommand();

    EmployeeToEmployeeCommand converterEmployee = new EmployeeToEmployeeCommand();

    @RequestMapping("/project/list")
    public String getAllProjects(Model model){


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        GrantedAuthority roles = (GrantedAuthority) auth.getAuthorities().toArray()[0];
        String role = roles.getAuthority();

        if(("ADMIN").equals(role))
        {
            model.addAttribute("projects",projectService.findAll());

        }
        if(("DEVELOPER").equals(role))
        {
            model.addAttribute("projects",employeeService.findEmployeeByEmail(auth.getName()).getProjects());
        }


        return"/project/list";
    }

    @RequestMapping("/project/{id}")
    public String getProjectId(Model model, @PathVariable Long id){

        Project project =projectService.findById(id);


        model.addAttribute("projects", Collections.singletonList(project));
        model.addAttribute("teams",project.getTeams());
        model.addAttribute("employees",project.getEmployees());

        return"/project/id";
    }

    @RequestMapping(value = "/project/create", method = RequestMethod.GET)
    public String getAddPage(Model model){

        if(!model.containsAttribute("project")) {
            model.addAttribute("project", new ProjectCommand());
        }
        return "/project/add";
    }

    @PostMapping("/project/create")
    public String save(@Valid @ModelAttribute("project") ProjectCommand project, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes){

        Project ifTeamExist = projectService.findByName(project.getName());

        if(ifTeamExist!=null){
            bindingResult.rejectValue("name","error.team","this name is already in use");
        }
        if(bindingResult.hasErrors())
        {
            System.out.println(bindingResult.getAllErrors());
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.team",bindingResult);
            redirectAttributes.addFlashAttribute("project",project);
//            model.addAttribute("employee",command);
            return "redirect:/project/create";
        }
        else {

            Project team1 = projectService.saveProject(project);

            return "redirect:/project/" + team1.getId();
        }
    }

    @RequestMapping("/team/{teamId}/project/{projectId}/remove")
    public String removeTeamFromProject(Model model, @PathVariable Long teamId, @PathVariable Long projectId){

        Project project = projectService.findById(projectId);
        Team team = teamService.findById(teamId);
        System.out.println("teams before");
        project.getTeams().forEach(t -> System.out.println(t.getName()));
        project.getTeams().remove(team);
        System.out.println("teams after");
        project.getTeams().forEach(t -> System.out.println(t.getName()));

        project.getEmployees().removeIf(e -> !e.getTeams().contains(team) | !Collections.disjoint(e.getTeams(),project.getTeams()));

        Set<Employee> employees = project.getEmployees();

        for(Employee e : employees)
        {
            for(Task t : e.getTasks())
            {
                if(t.getProject().getId().equals(projectId))
                {
                    return "redirect:/project/"+projectId;
                }
            }
        }
        employees.forEach(employee -> employee.getProjects().removeIf(p -> p.getId().equals(projectId) ) );

//        employees.forEach(e->employeeService.saveEmployee(converterEmployee.convert(e)));


        projectService.saveProject(converter.convert(project));
        return"redirect:/project/"+projectId;
    }

    @RequestMapping("/project/{projectId}/team/{teamId}/remove")
    public String removeProjectFromTeam(Model model, @PathVariable Long teamId, @PathVariable Long projectId){

        String s = removeTeamFromProject(model,teamId,projectId);

        return"redirect:/team/"+teamId;
    }


    @RequestMapping("/project/{projectId}/team/add")
    public String addTeamsToProject(Model model, @PathVariable Long projectId){

        Set<Team> allTeams = teamService.findAll();
        allTeams.removeAll(projectService.findById(projectId).getTeams());


        model.addAttribute("teams",allTeams);
        model.addAttribute("projectId",projectId);

        return"/project/team/new";
    }

    @RequestMapping("/project/{projectId}/team/add/{teamId}")
    public String addTeamToProject(Model model, @PathVariable Long teamId, @PathVariable Long projectId)
    {
        Project project = projectService.findById(projectId);
        project.getTeams().add(teamService.findById(teamId));

        projectService.saveProject(converter.convert(project));
        return"redirect:/project/"+projectId;
    }

    @RequestMapping("/project/{projectId}/employee/add")
    public String addEmployeeToProject(Model model, @PathVariable Long projectId){

        Project project = projectService.findById(projectId);
        Set<Team> teams = project.getTeams();
        Set<Employee> allEmployeesInTeam = new HashSet<>();
        teams.forEach(t-> allEmployeesInTeam.addAll(t.getEmployees()));
        allEmployeesInTeam.removeIf(e-> project.getEmployees().contains(e));

        model.addAttribute("employees",allEmployeesInTeam);
        return"/project/employee/new";
    }

    @RequestMapping("/project/{projectId}/employee/add/{employeeId}")
    public String addedEmployeeToproject(Model model, @PathVariable Long projectId,@PathVariable Long employeeId){

        Employee employee = employeeService.findById(employeeId);
        employee.getProjects().add(projectService.findById(projectId));
        employeeService.saveEmployee(converterEmployee.convert(employee));

        return"redirect:/project/"+projectId;
    }

    @RequestMapping("/employee/{employeeId}/project/{projectId}/remove")
    public String removeEmployeeFromProject(Model model, @PathVariable Long projectId,@PathVariable Long employeeId){

        Employee employee = employeeService.findById(employeeId);
        employee.getProjects().removeIf(p -> projectId.equals(p.getId()));
        employeeService.saveEmployee(converterEmployee.convert(employee));

        return"redirect:/project/"+projectId;
    }


    @Nullable
    @RequestMapping("/project/{projectId}/sprints")
    public String getSprintsIndex(Model model, @PathVariable Long projectId){

        Project project = projectService.findById(projectId);


        Set<Sprint> sprintNames = new HashSet<>();



        Set<Sprint> sprint = new HashSet<>();//new Sprint();// = null;
        for (Sprint s : project.getSprints()) {

            sprintNames.add(s);
            if(s.getIsActive())
            {
               sprint.add(s);
            }
        }

        System.out.println("SPRINTY YO : "+ sprintNames.size());

        Set<Task> tasks = new HashSet<>();

        if(sprint.iterator().hasNext()) {

            Optional<Set<Task>> taskOptional = Optional.of(sprint.iterator().next().getTasks());

            if(taskOptional.isPresent()){
                sprint.iterator().next().getTasks().forEach(t->tasks.add(t));
            }


        }

        Set<Task> todoTasks = tasks.stream()
                .filter(t-> (Status.TODO).equals(t.getStatus()))
                .collect(Collectors.toSet());
        Set<Task> inProgressTasks= tasks.stream()
                .filter(t-> (Status.IN_PROGRESS).equals(t.getStatus()))
                .collect(Collectors.toSet());
        Set<Task> inTestsTasks= tasks.stream()
                .filter(t-> (Status.IN_TEST).equals(t.getStatus()))
                .collect(Collectors.toSet());
        Set<Task> doneTasks= tasks.stream()
                .filter(t-> (Status.DONE).equals(t.getStatus()))
                .collect(Collectors.toSet());



        model.addAttribute("projects",project);
        model.addAttribute("sprintNames",sprintNames);
        model.addAttribute("sprints",sprint);
        model.addAttribute("todoTasks",todoTasks);
        model.addAttribute("inProgressTasks",inProgressTasks);
        model.addAttribute("inTestsTasks",inTestsTasks);
        model.addAttribute("doneTasks",doneTasks);
        model.addAttribute("name",new Sprint());



        return"/project/sprint/index";
    }

    @RequestMapping("/project/{projectId}/backlog")
    public String getBacklogIndex(Model model, @PathVariable Long projectId){

        Project project = projectService.findById(projectId);
        ProjectCommand projectCommand = converter.convert(project);
        Set<Task> tasksUnassigned = new HashSet<>();
        Set<Task> tasksAssigned = new HashSet<>();

        for (Task t: project.getTasks()) {

            if(t.getSprint() == null) {
                tasksUnassigned.add(t);
            }
            else
                tasksAssigned.add(t);
        }


        model.addAttribute("projects",Sets.newHashSet(projectCommand));
        model.addAttribute("tasksUnassigned",tasksUnassigned);
        model.addAttribute("tasksAssigned",tasksAssigned);

        return"/project/backlog/index";
    }


    @PostMapping("/sprint/id")
    public String getSprintByName(@Valid @ModelAttribute Sprint sprint){
        System.out.println("*******************************");
        System.out.println(sprint.getName());
        System.out.println("*******************************");

        return"";
    }

}
