package com.ksundaysky.agileboard.controllers;


import com.ksundaysky.agileboard.Services.EmployeeService;
import com.ksundaysky.agileboard.Services.ProjectService;
import com.ksundaysky.agileboard.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class BoardController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    ProjectService projectService;


    @RequestMapping("/myboard")
    public String getMyBoard(Model model)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        GrantedAuthority roles = (GrantedAuthority) auth.getAuthorities().toArray()[0];
        String role = roles.getAuthority();

        Employee employee = employeeService.findEmployeeByEmail(auth.getName());



        Set<Project> sprintNames = new HashSet<>();



        for (Project s : employee.getProjects()) {

            sprintNames.add(s);

        }


        Set<Task> tasks = new HashSet<>();

        employee.getTasks().forEach(t-> tasks.add(t));

        Set<Task> todoTasks = tasks.stream()
                .filter(t-> (Status.TODO).equals(t.getStatus()))
                .collect(Collectors.toSet());
        Set<Task> inProgressTasks= tasks.stream()
                .filter(t-> (Status.IN_PROGRESS).equals(t.getStatus()))
                .collect(Collectors.toSet());
        Set<Task> inTestsTasks= tasks.stream()
                .filter(t-> (Status.IN_TEST).equals(t.getStatus()))
                .collect(Collectors.toSet());
        Set<Task> doneTasks= tasks.stream()
                .filter(t-> (Status.DONE).equals(t.getStatus()))
                .collect(Collectors.toSet());



//        model.addAttribute("projects",project);
        model.addAttribute("projectNames",sprintNames);
//        model.addAttribute("sprints",sprint);
        model.addAttribute("todoTasks",todoTasks);
        model.addAttribute("inProgressTasks",inProgressTasks);
        model.addAttribute("inTestsTasks",inTestsTasks);
        model.addAttribute("doneTasks",doneTasks);
        model.addAttribute("name",new Sprint());
//        model.addAttribute("mainSprint", sprint);


//        if(("DEVELOPER").equals(role))
//        {
//            model.addAttribute("tasks",employee.getTasks());
//        }
        return "/employee/board";
    }
}
