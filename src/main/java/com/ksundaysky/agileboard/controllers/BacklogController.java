package com.ksundaysky.agileboard.controllers;


import com.ksundaysky.agileboard.Services.EmployeeService;
import com.ksundaysky.agileboard.Services.ProjectService;
import com.ksundaysky.agileboard.Services.TaskService;
import com.ksundaysky.agileboard.Services.TeamService;
import com.ksundaysky.agileboard.converters.EmployeeToEmployeeCommand;
import com.ksundaysky.agileboard.converters.ProjectToProjectCommand;
import com.ksundaysky.agileboard.domain.Project;
import com.ksundaysky.agileboard.domain.Sprint;
import com.ksundaysky.agileboard.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BacklogController {

    @Autowired
    ProjectService projectService;

    @Autowired
    TeamService teamService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    TaskService taskService;

    ProjectToProjectCommand converter = new ProjectToProjectCommand();

    EmployeeToEmployeeCommand converterEmployee = new EmployeeToEmployeeCommand();

    @RequestMapping("/project/{projectId}/sprints/task/{taskId}/add")
    public String addTaskToActiveSprint(Model model, @PathVariable Long projectId, @PathVariable Long taskId){

        Task task  = taskService.findById(taskId);
        Project project = projectService.findById(projectId);

        Sprint sprint = project.getSprints().stream()
                            .filter(s -> s.getIsActive().equals(true))
                            .findFirst()
                            .get();


        task.setSprint(sprint);

        taskService.save(task);
        return"redirect:/project/"+projectId+"/backlog";
    }



}
