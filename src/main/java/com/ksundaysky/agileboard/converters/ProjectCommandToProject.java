package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.domain.Project;
import org.springframework.core.convert.converter.Converter;
//import org.springframework.lang.Nullable;

public class ProjectCommandToProject implements Converter<ProjectCommand,Project> {

    public TeamCommandToTeam teamConverter;
    public TaskCommandToTask taskConverter;

    public ProjectCommandToProject() {
        this.teamConverter = new TeamCommandToTeam();
        this.taskConverter = new TaskCommandToTask();
    }

//    @Nullable
    @Override
    public Project convert(ProjectCommand source) {
        if(source == null){
            return null;
        }

        final Project project = new Project();

        project.setId(source.getId());
        project.setName(source.getName());
        project.setDescription(source.getDescription());
        if(source.getTeams()!=null && source.getTeams().size()>0){
            source.getTeams()
                    .forEach(teamCommand -> project.getTeams().add(teamConverter.convert(teamCommand)));
        }

        if(source.getTasks()!=null && source.getTasks().size()>0){
            source.getTasks()
                    .forEach(taskCommand -> project.getTasks().add(taskConverter.convert(taskCommand)));
        }


        return project;
    }
}
