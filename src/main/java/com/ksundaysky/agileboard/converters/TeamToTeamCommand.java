package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.TeamCommand;
import com.ksundaysky.agileboard.domain.Team;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;


public class TeamToTeamCommand implements Converter<Team,TeamCommand> {
    @Nullable
    @Override
    public TeamCommand convert(Team source) {

        if(source == null){
            return null;
        }

        final TeamCommand teamCommand = new TeamCommand();

        teamCommand.setId(source.getId());
        teamCommand.setName(source.getName());
        teamCommand.setHowManyMembers(source.getEmployees().size());
        teamCommand.setHowManyProjects(source.getProjects().size());
        teamCommand.setDescription(source.getDescription());
        return teamCommand;
    }
}
