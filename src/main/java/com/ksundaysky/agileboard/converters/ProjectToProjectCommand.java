package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.ProjectCommand;
import com.ksundaysky.agileboard.domain.Project;
import org.springframework.core.convert.converter.Converter;
//import org.springframework.lang.Nullable;

public class ProjectToProjectCommand implements Converter<Project,ProjectCommand> {

    public TeamToTeamCommand teamConverter;
    private TaskToTaskCommand taskConverter;
//    private EmployeeToEmployeeCommand employeeConverter;

    public ProjectToProjectCommand() {
        this.teamConverter = new TeamToTeamCommand();
        this.taskConverter = new TaskToTaskCommand();
//        this.employeeConverter = new EmployeeToEmployeeCommand();
    }

//    @Nullable
    @Override
    public ProjectCommand convert(Project source) {

        if(source == null){
            return null;
        }

        final ProjectCommand projectCommand = new ProjectCommand();

        projectCommand.setId(source.getId());
        projectCommand.setName(source.getName());
        projectCommand.setDescription(source.getDescription());
        if(source.getTeams()!=null && source.getTeams().size()>0){
            source.getTeams()
                    .forEach(team -> projectCommand.getTeams().add(teamConverter.convert(team)));
        }


        if(source.getTasks()!=null && source.getTasks().size()>0){
            source.getTasks()
                    .forEach(task -> projectCommand.getTasks().add(taskConverter.convert(task)));
        }
//        if(source.getEmployees()!=null && source.getEmployees().size()>0){
//            source.getEmployees()
//                    .forEach(employee -> projectCommand.getEmployees().add(employeeConverter.convert(employee)));
//        }


        return projectCommand;
    }
}
