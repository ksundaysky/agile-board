package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.SprintCommand;
import com.ksundaysky.agileboard.commands.TaskCommand;
import com.ksundaysky.agileboard.domain.Priority;
import com.ksundaysky.agileboard.domain.Task;
import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
//import org.springframework.lang.Nullable;

public class TaskCommandToTask implements Converter<TaskCommand,Task> {

//    private EmployeeCommandToEmployee employeeConverter;
//    private ProjectCommandToProject projectConverter;
//    private SprintCommandToSprint sprintConverter;

    public TaskCommandToTask() {
//        this.employeeConverter = new EmployeeCommandToEmployee();
//        this.projectConverter = new ProjectCommandToProject();
//        this.sprintConverter = new SprintCommandToSprint();
    }

//    @Nullable
    @Override
    public Task convert(TaskCommand source) {

        if(source == null){
            return null;
        }

        final Task task = new Task();

        task.setId(source.getId());
        task.setName(source.getName());
        task.setStatus(source.getStatus());
//        task.setPriority(source.getPriority());

        switch (source.getPriority()){
            case "LOW": task.setPriority(Priority.LOW);
                break;
            case "MEDIUM": task.setPriority(Priority.MEDIUM);
                break;
            case "HIGH": task.setPriority(Priority.HIGH);
                break;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            if(source.getDateWhenDone() != null && !source.getDateWhenDone().equals("")) {
                Date done = sdf.parse(source.getDateWhenDone());
                task.setDateWhenDone(new java.sql.Date(done.getTime()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        task.setEmployee(source.getEmployee());
        task.setProject(source.getProject());
        task.setSprint(source.getSprint());
        task.setIsAssigned(source.getIsAssigned());
//        task.setEmployee(employeeConverter.convert(source.getEmployee()));
//        task.setProject(projectConverter.convert((source.getProject())));
//        task.setSprint(sprintConverter.convert(source.getSprint()));
        return task;
    }
}
