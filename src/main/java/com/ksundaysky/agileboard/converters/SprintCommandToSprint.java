package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.Services.ProjectService;
import com.ksundaysky.agileboard.Services.SprintService;
import com.ksundaysky.agileboard.commands.SprintCommand;
import com.ksundaysky.agileboard.domain.Project;
import com.ksundaysky.agileboard.domain.Sprint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class SprintCommandToSprint implements Converter<SprintCommand,Sprint> {



        private ProjectCommandToProject converterProject;
        private TaskCommandToTask converterTask;

        public SprintCommandToSprint() {
            this.converterProject = new ProjectCommandToProject();
            this.converterTask = new TaskCommandToTask();
        }

        @Nullable
        @Override
        public Sprint convert(SprintCommand sprintCommand) {

            if(sprintCommand == null){
                return null;
            }

            Sprint sprint= new Sprint();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date startDate = sdf.parse(sprintCommand.getStartOfSprint());
                Date endDate = sdf.parse(sprintCommand.getEndOfSprint());
                sprint.setStartOfSprint(new java.sql.Date(startDate.getTime()));
                sprint.setEndOfSprint(new java.sql.Date(endDate.getTime()));

            } catch (ParseException e) {
                e.printStackTrace();
            }

            sprint.setId(sprintCommand.getId());
            sprint.setName(sprintCommand.getName());


//            Project project = projectService.findById(sprintCommand.getProjectId());
//            sprint.setProject(project);
//            System.out.println(sprintCommand.getProjectId());
//            sprint.setProject(projectService.findById(sprintCommand.getProjectId()));

            sprint.setProject(sprintCommand.getProject());

            if(sprintCommand.getTasks() !=null && sprintCommand.getTasks().size() >0){
                sprintCommand.getTasks()
                        .forEach(t->sprint.getTasks().add(converterTask.convert(t)));
            }

            sprint.setIsActive(sprintCommand.getIsActive());

            return sprint;
        }


}
