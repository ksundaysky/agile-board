package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.EmployeeCommand;
import com.ksundaysky.agileboard.domain.Employee;
import com.ksundaysky.agileboard.domain.Role;
import org.springframework.core.convert.converter.Converter;
//import org.springframework.lang.Nullable;

public class EmployeeToEmployeeCommand implements Converter<Employee,EmployeeCommand> {

    private ProjectToProjectCommand projectConverter;
    private TeamToTeamCommand teamConverter;

    public EmployeeToEmployeeCommand() {
        this.projectConverter = new ProjectToProjectCommand();
        this.teamConverter = new TeamToTeamCommand();
    }

//    @Nullable
    @Override
    public EmployeeCommand convert(Employee source) {

        if(source == null){
            return null;
        }

        final EmployeeCommand employeeCommand = new EmployeeCommand();
        employeeCommand.setId(source.getId());
        employeeCommand.setEmail(source.getEmail());
        employeeCommand.setFirstName(source.getFirstName());
        employeeCommand.setLastName(source.getLastName());
        employeeCommand.setPesel(source.getPesel().toString());
        //employeeCommand.setProjects(source.getProjects());
        if(source.getProjects() != null && source.getProjects().size() > 0){
            source.getProjects()
                    .forEach(project -> employeeCommand.getProjects().add(projectConverter.convert(project)));
        }

        if(source.getTeams()!=null && source.getTeams().size()>0){
            source.getTeams()
                    .forEach(team -> employeeCommand.getTeams().add(teamConverter.convert(team)));
        }

//        employeeCommand.setRole(source.getRole());
        if(source.getRole().equals(Role.ADMIN)) employeeCommand.setRole("ADMIN");
        else if(source.getRole().equals(Role.DEVELOPER)) employeeCommand.setRole("DEVELOPER");
        else if(source.getRole().equals(Role.PRODUCT_OWNER)) employeeCommand.setRole("PROJECT MANAGER");
        else if(source.getRole().equals(Role.SCRUM_MASTER)) employeeCommand.setRole("SCRUM MASTER");


        employeeCommand.setPassword(source.getPassword());
        return employeeCommand;
    }
}
