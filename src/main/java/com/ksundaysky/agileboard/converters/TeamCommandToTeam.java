package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.TeamCommand;
import com.ksundaysky.agileboard.domain.Team;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
//import org.springframework.lang.Nullable;

public class TeamCommandToTeam implements Converter<TeamCommand,Team> {

//    @Nullable
    @Override
    @Synchronized
    public Team convert(TeamCommand source) {

        if(source == null){
            return null;
        }

        final Team team = new Team();
        team.setId(source.getId());
        team.setName(source.getName());
        team.setDescription(source.getDescription());
        return team;
    }
}
