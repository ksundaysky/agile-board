package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.EmployeeCommand;
import com.ksundaysky.agileboard.domain.Employee;
import com.ksundaysky.agileboard.domain.Role;
import org.springframework.core.convert.converter.Converter;
//import org.springframework.lang.Nullable;

public class EmployeeCommandToEmployee implements Converter<EmployeeCommand,Employee> {

    private ProjectCommandToProject projectConverter;
    private TeamCommandToTeam teamConverter;


    public EmployeeCommandToEmployee() {
        this.projectConverter =new ProjectCommandToProject();
        this.teamConverter = new TeamCommandToTeam();
    }

//    @Nullable
    @Override
    public Employee convert(EmployeeCommand source) {

        if(source == null){
            return null;
        }

        final Employee employee = new Employee();

        employee.setId(source.getId());
        employee.setEmail(source.getEmail());
        employee.setFirstName(source.getFirstName());
        employee.setLastName(source.getLastName());
        employee.setPesel(source.getPesel());


        switch (source.getRole()){
            case "ADMIN": employee.setRole(Role.ADMIN);
                break;
            case "DEVELOPER": employee.setRole(Role.DEVELOPER);
                break;
            case "SCRUM MASTER": employee.setRole(Role.SCRUM_MASTER);
                break;
            case "PROJECT MANAGER": employee.setRole(Role.PRODUCT_OWNER);
        }

        if(source.getProjects()!=null && source.getProjects().size()>0){
            source.getProjects()
                    .forEach(projectCommand -> employee.getProjects().add(projectConverter.convert(projectCommand)));
        }
        if(source.getTeams()!=null && source.getTeams().size()>0){
            source.getTeams()
                    .forEach(teamCommand -> employee.getTeams().add(teamConverter.convert(teamCommand)));
        }

        employee.setPassword(source.getPassword());


        return employee;
    }
}
