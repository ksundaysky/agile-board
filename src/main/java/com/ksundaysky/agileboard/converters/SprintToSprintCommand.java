package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.SprintCommand;
import com.ksundaysky.agileboard.domain.Sprint;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SprintToSprintCommand implements Converter<Sprint,SprintCommand> {

    private ProjectToProjectCommand converterProject;
    private TaskToTaskCommand converterTask;

    public SprintToSprintCommand() {
        this.converterProject = new ProjectToProjectCommand();
        this.converterTask = new TaskToTaskCommand();
    }

    @Nullable
    @Override
    public SprintCommand convert(Sprint sprint) {

        if(sprint == null){
            return null;
        }

        SprintCommand sprintCommand = new SprintCommand();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date startDate = sdf.parse(sprint.getStartOfSprint().toString());
            Date endDate = sdf.parse(sprint.getEndOfSprint().toString());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        sprintCommand.setId(sprint.getId());
        sprintCommand.setName(sprint.getName());

        sprintCommand.setStartOfSprint(sprint.getStartOfSprint().toString());
        sprintCommand.setEndOfSprint(sprint.getEndOfSprint().toString());

        sprintCommand.setProject(sprint.getProject());

        if(sprint.getTasks() !=null && sprint.getTasks().size() >0){
            sprint.getTasks()
                    .forEach(t->sprintCommand.getTasks().add(converterTask.convert(t)));
        }

        return sprintCommand;
    }
}
