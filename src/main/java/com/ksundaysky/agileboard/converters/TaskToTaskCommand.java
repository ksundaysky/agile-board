package com.ksundaysky.agileboard.converters;

import com.ksundaysky.agileboard.commands.TaskCommand;
import com.ksundaysky.agileboard.domain.Sprint;
import com.ksundaysky.agileboard.domain.Task;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
//import org.springframework.lang.Nullable;

public class TaskToTaskCommand implements Converter<Task,TaskCommand> {

//    private EmployeeToEmployeeCommand employeeConverter;
//    private ProjectToProjectCommand projectConverter;
//    private SprintToSprintCommand sprintConverter;

    public TaskToTaskCommand() {
//        this.employeeConverter = new EmployeeToEmployeeCommand();
//        this.projectConverter = new ProjectToProjectCommand();
//        this.sprintConverter = new SprintToSprintCommand();
    }

    @Nullable
    @Override
    public TaskCommand convert(Task source) {


        if(source == null){
            return null;
        }

        final TaskCommand taskCommand = new TaskCommand();

        taskCommand.setId(source.getId());
        taskCommand.setName(source.getName());
        taskCommand.setStatus(source.getStatus());
        taskCommand.setPriority(source.getPriority().toString());
        if (source.getDateWhenDone() == null)
            taskCommand.setDateWhenDone("");
        else
            taskCommand.setDateWhenDone(source.getDateWhenDone().toString());

        taskCommand.setEmployee(source.getEmployee());
        taskCommand.setProject(source.getProject());
        taskCommand.setSprint(source.getSprint());
        taskCommand.setIsAssigned(source.getIsAssigned());
//        taskCommand.setEmployee(employeeConverter.convert(source.getEmployee()));
//        taskCommand.setProject(projectConverter.convert(source.getProject()));
//        taskCommand.setSprint(sprintConverter.convert(source.getSprint()));
        return taskCommand;
    }
}
