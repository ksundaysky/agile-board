package com.ksundaysky.agileboard.bootstrap;


import com.google.common.collect.Sets;
import com.ksundaysky.agileboard.domain.*;
import com.ksundaysky.agileboard.repositories.EmployeeRepository;
import com.ksundaysky.agileboard.repositories.ProjectRepository;
import com.ksundaysky.agileboard.repositories.TaskRepository;
import com.ksundaysky.agileboard.repositories.TeamRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.expression.Lists;

import java.lang.reflect.Array;
import java.util.*;

//@Slf4j
@Component
public class EmployeeBootstrap implements ApplicationListener<ContextRefreshedEvent>
{


    private final EmployeeRepository employeeRepository;
    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    private final TeamRepository teamRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public EmployeeBootstrap(EmployeeRepository employeeRepository, ProjectRepository projectRepository, TaskRepository taskRepository, TeamRepository teamRepository) {
        this.employeeRepository = employeeRepository;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.teamRepository = teamRepository;
    }

    @Override
    @Transactional

    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
//        log.info("Im in bootstrap class!");
        employeeRepository.saveAll(prepareData());

    }

    private List<Employee> prepareData()
    {

        List<Employee> employees = new LinkedList<>();

        //Project BankingSystemHSBC

        Optional<Project> bankingSystemHSBCProjectOptional = projectRepository.findByName("BankingSystemHSBC");

        if(!bankingSystemHSBCProjectOptional.isPresent())
        {
            throw  new RuntimeException("Expected Team not found");
        }

        Project bankingSystemHSBCProject = bankingSystemHSBCProjectOptional.get();


        //Project Online Shop DecathlonC

        Optional<Project> onlineShopDecathlonProjectOptional = projectRepository.findByName("OnlineShopDecathlon");

        if(!onlineShopDecathlonProjectOptional.isPresent())
        {
            throw  new RuntimeException("Expected Team not found");
        }

        Project onlineShopDecathlonProject = onlineShopDecathlonProjectOptional.get();


        //Project Risk Management UBS

        Optional<Project> riskManagementUBSProjectOptional = projectRepository.findByName("RiskManagementUBS");

        if(!riskManagementUBSProjectOptional.isPresent())
        {
            throw  new RuntimeException("Expected Team not found");
        }

        Project riskManagementUBSProject = riskManagementUBSProjectOptional.get();

        //Team Fast and Furious


        Optional<Team> fastAndFuriousTeamOptional = teamRepository.findByName("FastAndFurious");

        if(!fastAndFuriousTeamOptional.isPresent())
        {
            throw  new RuntimeException("Expected Team not found");
        }

        Team fastAndFuriousTeam = fastAndFuriousTeamOptional.get();


        //Team Crazy Frogs


        Optional<Team> crazyFrogsTeamOptional = teamRepository.findByName("CrazyFrogs");

        if(!crazyFrogsTeamOptional.isPresent())
        {
            throw  new RuntimeException("Expected Team not found");
        }

        Team crazyFrogsTeam = crazyFrogsTeamOptional.get();


        //Team Unicorns


        Optional<Team> unicornsTeamOptional = teamRepository.findByName("Unicorns");

        if(!unicornsTeamOptional.isPresent())
        {
            throw  new RuntimeException("Expected Team not found");
        }

        Team unicornsTeam = unicornsTeamOptional.get();


        //Team BeerLovers


        Optional<Team> beerLoversTeamOptional = teamRepository.findByName("BeerLovers");

        if(!beerLoversTeamOptional.isPresent())
        {
            throw  new RuntimeException("Expected Team not found");
        }

        Team beerLoversTeam = beerLoversTeamOptional.get();


        //Team BeerLovers


        Optional<Team> dataFreaksTeamOptional = teamRepository.findByName("DataFreaks");

        if(!dataFreaksTeamOptional.isPresent())
        {
            throw  new RuntimeException("Expected Team not found");
        }

        Team dataFreaksTeam = dataFreaksTeamOptional.get();



        // add fast and furious and crazyfrogs to bankingSystemHSBC
        bankingSystemHSBCProject.getTeams().addAll(Arrays.asList(fastAndFuriousTeam,crazyFrogsTeam));

        //add unicorns to onlineShopDecathlonProjectOptional

        onlineShopDecathlonProject.getTeams().addAll(Arrays.asList(unicornsTeam));

        //add beerlovers to riskManagementUBSProject

        riskManagementUBSProject.getTeams().addAll(Arrays.asList(beerLoversTeam,dataFreaksTeam));

        //ADMIN
        Employee adminEmployee = employeeRepository.findById(1L).get(); // new Employee();

        adminEmployee.setFirstName("Admin");
        adminEmployee.setLastName("Admin");
        adminEmployee.setEmail("admin@admin.pl");
        adminEmployee.setPesel("96030306058");
        adminEmployee.setRole(Role.ADMIN);
        adminEmployee.setPassword(bCryptPasswordEncoder.encode("admin"));


        // Employee John Doe Team Fast and Furious

        Employee johnDoeEmployeeTeamFast = employeeRepository.findById(2L).get();

        johnDoeEmployeeTeamFast.setFirstName("John");
        johnDoeEmployeeTeamFast.setLastName("Doe");
        johnDoeEmployeeTeamFast.setEmail("john.doe@internal-email.com");
        johnDoeEmployeeTeamFast.setPesel("81030305034");
        johnDoeEmployeeTeamFast.setRole(Role.SCRUM_MASTER);
        johnDoeEmployeeTeamFast.setPassword(bCryptPasswordEncoder.encode("admin"));
        johnDoeEmployeeTeamFast.getTeams().add(fastAndFuriousTeam);
        johnDoeEmployeeTeamFast.getTeams().add(crazyFrogsTeam);
        johnDoeEmployeeTeamFast.getProjects().add(bankingSystemHSBCProject);

        // Employee Foo Bar Team Fast and Furious

        Employee fooBarEmployeeTeamFast = employeeRepository.findById(3L).get();

        fooBarEmployeeTeamFast.setFirstName("Foo");
        fooBarEmployeeTeamFast.setLastName("Bar");
        fooBarEmployeeTeamFast.setEmail("foo.bar@internal-email.com");
        fooBarEmployeeTeamFast.setPesel("82040306034");
        fooBarEmployeeTeamFast.setRole(Role.DEVELOPER);
        fooBarEmployeeTeamFast.setPassword(bCryptPasswordEncoder.encode("admin"));
        fooBarEmployeeTeamFast.getTeams().add(fastAndFuriousTeam);
        fooBarEmployeeTeamFast.getProjects().add(bankingSystemHSBCProject);



        // Employee For Loop Team Fast and Furious

        Employee forLoopEmployeeTeamFast = employeeRepository.findById(4L).get();

        forLoopEmployeeTeamFast.setFirstName("For");
        forLoopEmployeeTeamFast.setLastName("Loop");
        forLoopEmployeeTeamFast.setEmail("for.loop@internal-email.com");
        forLoopEmployeeTeamFast.setPesel("89030609034");
        forLoopEmployeeTeamFast.setRole(Role.DEVELOPER);
        forLoopEmployeeTeamFast.setPassword(bCryptPasswordEncoder.encode("admin"));
        forLoopEmployeeTeamFast.getTeams().add(fastAndFuriousTeam);
        forLoopEmployeeTeamFast.getProjects().add(bankingSystemHSBCProject);



        // Employee Johny English Team Fast and Furious

        Employee johnyEnglishEmployeeTeamFast = employeeRepository.findById(5L).get();

        johnyEnglishEmployeeTeamFast.setFirstName("Johny");
        johnyEnglishEmployeeTeamFast.setLastName("English");
        johnyEnglishEmployeeTeamFast.setEmail("johny.english@internal-email.com");
        johnyEnglishEmployeeTeamFast.setPesel("79030709114");
        johnyEnglishEmployeeTeamFast.setRole(Role.DEVELOPER);
        johnyEnglishEmployeeTeamFast.setPassword(bCryptPasswordEncoder.encode("admin"));
        johnyEnglishEmployeeTeamFast.getTeams().add(fastAndFuriousTeam);
        johnyEnglishEmployeeTeamFast.getProjects().add(bankingSystemHSBCProject);


        // Employee Angelina Jolie Team Fast and Furious

        Employee angelinaJolieEmployeeTeamFast = employeeRepository.findById(6L).get();

        angelinaJolieEmployeeTeamFast.setFirstName("Angelina");
        angelinaJolieEmployeeTeamFast.setLastName("Jolie");
        angelinaJolieEmployeeTeamFast.setEmail("angelina.jolie@internal-email.com");
        angelinaJolieEmployeeTeamFast.setPesel("75030609114");
        angelinaJolieEmployeeTeamFast.setRole(Role.DEVELOPER);
        angelinaJolieEmployeeTeamFast.setPassword(bCryptPasswordEncoder.encode("admin"));
        angelinaJolieEmployeeTeamFast.getTeams().add(fastAndFuriousTeam);
        angelinaJolieEmployeeTeamFast.getProjects().add(bankingSystemHSBCProject);


        // Employee Brat Pitt Team Fast and Furious

        Employee bratPittEmployeeTeamFast = employeeRepository.findById(7L).get();

        bratPittEmployeeTeamFast.setFirstName("Brat");
        bratPittEmployeeTeamFast.setLastName("Pitt");
        bratPittEmployeeTeamFast.setEmail("brat.pitt@internal-email.com");
        bratPittEmployeeTeamFast.setPesel("72040709114");
        bratPittEmployeeTeamFast.setRole(Role.PRODUCT_OWNER);
        bratPittEmployeeTeamFast.setPassword(bCryptPasswordEncoder.encode("admin"));
        bratPittEmployeeTeamFast.getTeams().add(fastAndFuriousTeam);
        bratPittEmployeeTeamFast.getTeams().add(crazyFrogsTeam);
        bratPittEmployeeTeamFast.getProjects().add(bankingSystemHSBCProject);


        // Employee George Cluney Team CrazyFrogs

        Employee georgeCluneyEmployeeTeamCrazy = employeeRepository.findById(8L).get();

        georgeCluneyEmployeeTeamCrazy.setFirstName("George");
        georgeCluneyEmployeeTeamCrazy.setLastName("Cluney");
        georgeCluneyEmployeeTeamCrazy.setEmail("george.cluney@internal-email.com");
        georgeCluneyEmployeeTeamCrazy.setPesel("63050709114");
        georgeCluneyEmployeeTeamCrazy.setRole(Role.DEVELOPER);
        georgeCluneyEmployeeTeamCrazy.setPassword(bCryptPasswordEncoder.encode("admin"));
        georgeCluneyEmployeeTeamCrazy.getTeams().add(crazyFrogsTeam);
        georgeCluneyEmployeeTeamCrazy.getProjects().add(bankingSystemHSBCProject);

        // Employee Britney Spears Team CrazyFrogs

        Employee britneySpearsEmployeeTeamCrazy =employeeRepository.findById(9L).get();

        britneySpearsEmployeeTeamCrazy.setFirstName("Britney");
        britneySpearsEmployeeTeamCrazy.setLastName("Spears");
        britneySpearsEmployeeTeamCrazy.setEmail("briney.spears@internal-email.com");
        britneySpearsEmployeeTeamCrazy.setPesel("71050909254");
        britneySpearsEmployeeTeamCrazy.setRole(Role.DEVELOPER);
        britneySpearsEmployeeTeamCrazy.setPassword(bCryptPasswordEncoder.encode("admin"));
        britneySpearsEmployeeTeamCrazy.getTeams().add(crazyFrogsTeam);
        britneySpearsEmployeeTeamCrazy.getProjects().add(bankingSystemHSBCProject);


        // Employee Johny Deep Team CrazyFrogs

        Employee johnyDeepEmployeeTeamCrazy = employeeRepository.findById(10L).get();

        johnyDeepEmployeeTeamCrazy.setFirstName("Johny");
        johnyDeepEmployeeTeamCrazy.setLastName("Deep");
        johnyDeepEmployeeTeamCrazy.setEmail("johny.deep@internal-email.com");
        johnyDeepEmployeeTeamCrazy.setPesel("74060934254");
        johnyDeepEmployeeTeamCrazy.setRole(Role.DEVELOPER);
        johnyDeepEmployeeTeamCrazy.setPassword(bCryptPasswordEncoder.encode("admin"));
        johnyDeepEmployeeTeamCrazy.getTeams().add(crazyFrogsTeam);
        johnyDeepEmployeeTeamCrazy.getProjects().add(bankingSystemHSBCProject);

        // Employee Jack Sparrow Team CrazyFrogs

        Employee jackSparrowEmployeeTeamCrazy = employeeRepository.findById(11L).get();

        jackSparrowEmployeeTeamCrazy.setFirstName("Jack");
        jackSparrowEmployeeTeamCrazy.setLastName("Sparrow");
        jackSparrowEmployeeTeamCrazy.setEmail("jack.sparrow@internal-email.com");
        jackSparrowEmployeeTeamCrazy.setPesel("45080124254");
        jackSparrowEmployeeTeamCrazy.setRole(Role.DEVELOPER);
        jackSparrowEmployeeTeamCrazy.setPassword(bCryptPasswordEncoder.encode("admin"));
        jackSparrowEmployeeTeamCrazy.getTeams().add(crazyFrogsTeam);
        jackSparrowEmployeeTeamCrazy.getProjects().add(bankingSystemHSBCProject);


        bankingSystemHSBCProject.getEmployees().addAll(Arrays.asList(jackSparrowEmployeeTeamCrazy,johnyDeepEmployeeTeamCrazy,britneySpearsEmployeeTeamCrazy
                ,georgeCluneyEmployeeTeamCrazy,bratPittEmployeeTeamFast,angelinaJolieEmployeeTeamFast,johnyEnglishEmployeeTeamFast,
                forLoopEmployeeTeamFast,fooBarEmployeeTeamFast,johnDoeEmployeeTeamFast));
        // Employee John Travolta Team Unicorns

        Employee johnTravoltaEmployeeTeamUnicorn = employeeRepository.findById(12L).get();

        johnTravoltaEmployeeTeamUnicorn.setFirstName("John");
        johnTravoltaEmployeeTeamUnicorn.setLastName("Travolta");
        johnTravoltaEmployeeTeamUnicorn.setEmail("john.travolta@internal-email.com");
        johnTravoltaEmployeeTeamUnicorn.setPesel("56091324254");
        johnTravoltaEmployeeTeamUnicorn.setRole(Role.DEVELOPER);
        johnTravoltaEmployeeTeamUnicorn.setPassword(bCryptPasswordEncoder.encode("admin"));
        johnTravoltaEmployeeTeamUnicorn.getTeams().add(unicornsTeam);
        johnTravoltaEmployeeTeamUnicorn.getProjects().add(onlineShopDecathlonProject);



        // Employee Pablo Escobar Team Unicorns

        Employee pabloEscobarEmployeeTeamUnicorn = employeeRepository.findById(13L).get();

        pabloEscobarEmployeeTeamUnicorn.setFirstName("Pablo");
        pabloEscobarEmployeeTeamUnicorn.setLastName("Escobar");
        pabloEscobarEmployeeTeamUnicorn.setEmail("pablo.escobar@internal-email.com");
        pabloEscobarEmployeeTeamUnicorn.setPesel("51101212654");
        pabloEscobarEmployeeTeamUnicorn.setRole(Role.DEVELOPER);
        pabloEscobarEmployeeTeamUnicorn.setPassword(bCryptPasswordEncoder.encode("admin"));
        pabloEscobarEmployeeTeamUnicorn.getTeams().add(unicornsTeam);
        pabloEscobarEmployeeTeamUnicorn.getProjects().add(onlineShopDecathlonProject);


        // Employee Grzegorz Rasiak Team Unicorns

        Employee grzegorzRasiakEmployeeTeamUnicorn = employeeRepository.findById(14L).get();

        grzegorzRasiakEmployeeTeamUnicorn.setFirstName("Grzegorz");
        grzegorzRasiakEmployeeTeamUnicorn.setLastName("Rasiak");
        grzegorzRasiakEmployeeTeamUnicorn.setEmail("grzegorz.rasiak@internal-email.com");
        grzegorzRasiakEmployeeTeamUnicorn.setPesel("78121212653");
        grzegorzRasiakEmployeeTeamUnicorn.setRole(Role.DEVELOPER);
        grzegorzRasiakEmployeeTeamUnicorn.setPassword(bCryptPasswordEncoder.encode("admin"));
        grzegorzRasiakEmployeeTeamUnicorn.getTeams().add(unicornsTeam);
        grzegorzRasiakEmployeeTeamUnicorn.getProjects().add(onlineShopDecathlonProject);


        // Employee Al Capone Team Unicorns

        Employee alCaponeEmployeeTeamUnicorn = employeeRepository.findById(15L).get();

        alCaponeEmployeeTeamUnicorn.setFirstName("Al");
        alCaponeEmployeeTeamUnicorn.setLastName("Capone");
        alCaponeEmployeeTeamUnicorn.setEmail("al.capone@internal-email.com");
        alCaponeEmployeeTeamUnicorn.setPesel("33121315656");
        alCaponeEmployeeTeamUnicorn.setRole(Role.PRODUCT_OWNER);
        alCaponeEmployeeTeamUnicorn.setPassword(bCryptPasswordEncoder.encode("admin"));
        alCaponeEmployeeTeamUnicorn.getTeams().add(unicornsTeam);
//        alCaponeEmployeeTeamUnicorn.getProjects().add(onlineShopDecathlonProject);
        onlineShopDecathlonProject.getEmployees().addAll(Arrays.asList(alCaponeEmployeeTeamUnicorn,grzegorzRasiakEmployeeTeamUnicorn,pabloEscobarEmployeeTeamUnicorn,johnTravoltaEmployeeTeamUnicorn));
        // Employee Tomasz Kopyra Team BeerLovers

        Employee tomaszKopyraEmployeeTeamBeerLovers = employeeRepository.findById(16L).get();

        tomaszKopyraEmployeeTeamBeerLovers.setFirstName("Tomasz");
        tomaszKopyraEmployeeTeamBeerLovers.setLastName("Kopyra");
        tomaszKopyraEmployeeTeamBeerLovers.setEmail("tomasz.kopyra@internal-email.com");
        tomaszKopyraEmployeeTeamBeerLovers.setPesel("76060512446");
        tomaszKopyraEmployeeTeamBeerLovers.setRole(Role.DEVELOPER);
        tomaszKopyraEmployeeTeamBeerLovers.setPassword(bCryptPasswordEncoder.encode("admin"));
        tomaszKopyraEmployeeTeamBeerLovers.getTeams().add(beerLoversTeam);
        tomaszKopyraEmployeeTeamBeerLovers.getProjects().add(riskManagementUBSProject);


        // Employee Justin Bieber Team BeerLovers

        Employee justinBieberEmployeeTeamBeerLovers =employeeRepository.findById(17L).get();

        justinBieberEmployeeTeamBeerLovers.setFirstName("Justin");
        justinBieberEmployeeTeamBeerLovers.setLastName("Bieber");
        justinBieberEmployeeTeamBeerLovers.setEmail("justin.bieber@internal-email.com");
        justinBieberEmployeeTeamBeerLovers.setPesel("98030512426");
        justinBieberEmployeeTeamBeerLovers.setRole(Role.DEVELOPER);
        justinBieberEmployeeTeamBeerLovers.setPassword(bCryptPasswordEncoder.encode("admin"));
        justinBieberEmployeeTeamBeerLovers.getTeams().add(beerLoversTeam);
        justinBieberEmployeeTeamBeerLovers.getProjects().add(riskManagementUBSProject);


        // Employee Elon Musk Team BeerLovers

        Employee elonMuskEmployeeTeamBeerLovers = employeeRepository.findById(18L).get();

        elonMuskEmployeeTeamBeerLovers.setFirstName("Elon");
        elonMuskEmployeeTeamBeerLovers.setLastName("Musk");
        elonMuskEmployeeTeamBeerLovers.setEmail("elon.musk@internal-email.com");
        elonMuskEmployeeTeamBeerLovers.setPesel("63100512426");
        elonMuskEmployeeTeamBeerLovers.setRole(Role.PRODUCT_OWNER);
        elonMuskEmployeeTeamBeerLovers.setPassword(bCryptPasswordEncoder.encode("admin"));
        elonMuskEmployeeTeamBeerLovers.getTeams().add(beerLoversTeam);
        elonMuskEmployeeTeamBeerLovers.getTeams().add(dataFreaksTeam);
        elonMuskEmployeeTeamBeerLovers.getProjects().add(riskManagementUBSProject);

        // Employee Bruce Eckel Team BeerLovers

        Employee bruceEckelEmployeeTeamBeerLovers =employeeRepository.findById(19L).get();

        bruceEckelEmployeeTeamBeerLovers.setFirstName("Bruce");
        bruceEckelEmployeeTeamBeerLovers.setLastName("Eckel");
        bruceEckelEmployeeTeamBeerLovers.setEmail("bruce.eckel@internal-email.com");
        bruceEckelEmployeeTeamBeerLovers.setPesel("63230512426");
        bruceEckelEmployeeTeamBeerLovers.setRole(Role.DEVELOPER);
        bruceEckelEmployeeTeamBeerLovers.setPassword(bCryptPasswordEncoder.encode("admin"));
        bruceEckelEmployeeTeamBeerLovers.getTeams().add(beerLoversTeam);
        bruceEckelEmployeeTeamBeerLovers.getProjects().add(riskManagementUBSProject);


        // Employee Albus Dumbledore  Team DataFreaks

        Employee albusDumbledoreEmployeeTeamDataFreaks =employeeRepository.findById(20L).get();

        albusDumbledoreEmployeeTeamDataFreaks.setFirstName("Albus");
        albusDumbledoreEmployeeTeamDataFreaks.setLastName("Dumbledore");
        albusDumbledoreEmployeeTeamDataFreaks.setEmail("albus.dumbledore@internal-email.com");
        albusDumbledoreEmployeeTeamDataFreaks.setPesel("12103512426");
        albusDumbledoreEmployeeTeamDataFreaks.setRole(Role.SCRUM_MASTER);
        albusDumbledoreEmployeeTeamDataFreaks.setPassword(bCryptPasswordEncoder.encode("admin"));
        albusDumbledoreEmployeeTeamDataFreaks.getTeams().add(dataFreaksTeam);
        albusDumbledoreEmployeeTeamDataFreaks.getProjects().add(riskManagementUBSProject);

        // Employee Louis Hamilton  Team DataFreaks

        Employee louisHamiltonEmployeeTeamDataFreaks = employeeRepository.findById(21L).get();

        louisHamiltonEmployeeTeamDataFreaks.setFirstName("Louis");
        louisHamiltonEmployeeTeamDataFreaks.setLastName("Hamilton");
        louisHamiltonEmployeeTeamDataFreaks.setEmail("louis.hamilton@internal-email.com");
        louisHamiltonEmployeeTeamDataFreaks.setPesel("88032314123");
        louisHamiltonEmployeeTeamDataFreaks.setRole(Role.DEVELOPER);
        louisHamiltonEmployeeTeamDataFreaks.setPassword(bCryptPasswordEncoder.encode("admin"));
        louisHamiltonEmployeeTeamDataFreaks.getTeams().add(dataFreaksTeam);
        louisHamiltonEmployeeTeamDataFreaks.getProjects().add(riskManagementUBSProject);

        // Employee Meryl Strip Team DataFreaks

        Employee merylStripEmployeeTeamDataFreaks = employeeRepository.findById(22L).get();

        merylStripEmployeeTeamDataFreaks.setFirstName("Meryl");
        merylStripEmployeeTeamDataFreaks.setLastName("Strip");
        merylStripEmployeeTeamDataFreaks.setEmail("meryl.strip@internal-email.com");
        merylStripEmployeeTeamDataFreaks.setPesel("59032314123");
        merylStripEmployeeTeamDataFreaks.setRole(Role.DEVELOPER);
        merylStripEmployeeTeamDataFreaks.setPassword(bCryptPasswordEncoder.encode("admin"));
        merylStripEmployeeTeamDataFreaks.getTeams().add(dataFreaksTeam);
        merylStripEmployeeTeamDataFreaks.getProjects().add(riskManagementUBSProject);


        riskManagementUBSProject.getEmployees().addAll(Arrays.asList(merylStripEmployeeTeamDataFreaks,louisHamiltonEmployeeTeamDataFreaks,albusDumbledoreEmployeeTeamDataFreaks,
                bruceEckelEmployeeTeamBeerLovers,elonMuskEmployeeTeamBeerLovers,justinBieberEmployeeTeamBeerLovers,tomaszKopyraEmployeeTeamBeerLovers));

        //TASKS FOR BANKING HSBC

        Optional<Task> loginTaskOptional = taskRepository.findByName("Logging panel");

        if(!loginTaskOptional.isPresent())
        {
            throw  new RuntimeException("Expected Task not found");
        }

        Task loginTask = loginTaskOptional.get();
        loginTask.setPriority(Priority.HIGH);
        loginTask.setStatus(Status.TODO);
        loginTask.setProject(bankingSystemHSBCProject);

        Optional<Task> validTaskOptional = taskRepository.findByName("Valid login form");

        if(!validTaskOptional.isPresent())
        {
            throw  new RuntimeException("Expected Task not found");
        }

        Task validTask = validTaskOptional.get();
        validTask.setPriority(Priority.LOW);
        validTask.setStatus(Status.TODO);
        validTask.setProject(bankingSystemHSBCProject);

        Optional<Task> frontendTaskOptional = taskRepository.findByName("Frontend - form logging in");

        if(!frontendTaskOptional.isPresent())
        {
            throw  new RuntimeException("Expected Task not found");
        }

        Task frontendTask = frontendTaskOptional.get();
        frontendTask.setPriority(Priority.MEDIUM);
        frontendTask.setStatus(Status.TODO);
        frontendTask.setProject(bankingSystemHSBCProject);

        Optional<Task> restTaskOptional = taskRepository.findByName("REST controller for adding new customer");

        if(!restTaskOptional.isPresent())
        {
            throw  new RuntimeException("Expected Task not found");
        }

        Task restTask = restTaskOptional.get();
        restTask.setPriority(Priority.HIGH);
        restTask.setStatus(Status.TODO);
        restTask.setProject(bankingSystemHSBCProject);

        taskRepository.saveAll(Arrays.asList(restTask,frontendTask,loginTask,validTask));

//        bankingSystemHSBCProject.setTasks(Sets.newHashSet(restTask,frontendTask,loginTask,validTask));

//        projectRepository.save(bankingSystemHSBCProject);

        employees.addAll(Arrays.asList(adminEmployee,johnDoeEmployeeTeamFast,fooBarEmployeeTeamFast,johnyEnglishEmployeeTeamFast,angelinaJolieEmployeeTeamFast,bratPittEmployeeTeamFast,forLoopEmployeeTeamFast));
        employees.addAll(Arrays.asList(georgeCluneyEmployeeTeamCrazy,britneySpearsEmployeeTeamCrazy,johnyDeepEmployeeTeamCrazy,jackSparrowEmployeeTeamCrazy));
        employees.addAll(Arrays.asList(grzegorzRasiakEmployeeTeamUnicorn,alCaponeEmployeeTeamUnicorn,pabloEscobarEmployeeTeamUnicorn,johnTravoltaEmployeeTeamUnicorn));
        employees.addAll(Arrays.asList(bruceEckelEmployeeTeamBeerLovers,elonMuskEmployeeTeamBeerLovers,justinBieberEmployeeTeamBeerLovers,tomaszKopyraEmployeeTeamBeerLovers));
        employees.addAll(Arrays.asList(merylStripEmployeeTeamDataFreaks,louisHamiltonEmployeeTeamDataFreaks,albusDumbledoreEmployeeTeamDataFreaks));


        return employees;
    }

}
