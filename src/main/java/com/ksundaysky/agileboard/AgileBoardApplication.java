package com.ksundaysky.agileboard;

import com.ksundaysky.agileboard.bootstrap.EmployeeBootstrap;
import com.ksundaysky.agileboard.configuration.H2JpaConfig;
import lombok.ToString;
import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.sql.SQLException;

@SpringBootApplication
//@EnableAutoConfiguration(exclude = H2JpaConfig.class)
@SuppressWarnings("unchecked")
public class AgileBoardApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgileBoardApplication.class, args);
	}

}
