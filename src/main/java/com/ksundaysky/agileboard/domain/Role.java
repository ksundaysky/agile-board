package com.ksundaysky.agileboard.domain;

public enum Role {

    PRODUCT_OWNER,SCRUM_MASTER,DEVELOPER,ADMIN
}
