package com.ksundaysky.agileboard.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@EqualsAndHashCode(exclude = {"employees","projects"})
@Data
public class Team {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @ManyToMany(mappedBy = "teams")
    private Set<Employee> employees = new HashSet<>();

    @ManyToMany(mappedBy = "teams")
    private Set<Project> projects  = new HashSet<>();

}
