package com.ksundaysky.agileboard.domain;

import lombok.Data;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
//import org.hibernate.validator.constraints.NotEmpty;

import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NotEmpty(message = "Pleas provide your name ")
    private String firstName;
    @Column
    @NotEmpty(message = "Pleas provide your surname ")
    private String lastName;
    @Column
    private String pesel;

    @Column(name="email")
    @NotEmpty(message = "Provide your email")
    private String email;

    @NotEmpty(message = "Provide your password")
    private String password;

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(name = "employee_project",
    joinColumns = @JoinColumn(name ="employee_id"),
    inverseJoinColumns = @JoinColumn(name = "project_id"))
    private Set<Project> projects = new HashSet<>();

    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(name = "employee_team",
    joinColumns = @JoinColumn(name = "employee_id"),
    inverseJoinColumns = @JoinColumn(name = "team_id"))
    private Set<Team> teams = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employee")
    private Set<Task> tasks = new HashSet<>();

    public Employee addTask(Task task)
    {
        if(task != null){
            task.setEmployee(this);
            this.tasks.add(task);
            return this;
        }

        return null;
    }




}

