package com.ksundaysky.agileboard.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@EqualsAndHashCode(exclude = {"employees","teams","tasks","sprints"})
@Data
public class Project {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @ManyToMany(mappedBy = "projects")
    private Set<Employee> employees = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "project_team",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "team_id"))
    private Set<Team> teams = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "project")
    private Set<Task> tasks = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "project")
    private Set<Sprint> sprints = new HashSet<>();


    private Project addTask(Task task){
        if(task!=null) {
            task.setProject(this);
            this.tasks.add(task);
        }
        return null;
    }


}
