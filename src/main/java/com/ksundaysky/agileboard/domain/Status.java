package com.ksundaysky.agileboard.domain;

public enum Status {

    TODO,IN_PROGRESS,IN_TEST,DONE
}
