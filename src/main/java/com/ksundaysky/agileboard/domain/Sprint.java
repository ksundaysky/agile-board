package com.ksundaysky.agileboard.domain;


import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class Sprint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    private String name;

    private Boolean isActive;

    @Type(type = "date")
    private Date startOfSprint;

    @Type(type = "date")
    private Date endOfSprint;

    @ManyToOne
    private Project project;



    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sprint")
    private Set<Task> tasks = new HashSet<>();


}
