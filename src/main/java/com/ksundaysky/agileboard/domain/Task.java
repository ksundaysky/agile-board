package com.ksundaysky.agileboard.domain;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.sql.Date;


@Entity
@EqualsAndHashCode(exclude = {"employee","project","sprint"})
@Data
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Date dateWhenDone;

    private Priority priority;

    private Status status;

    private Boolean isAssigned;

    @ManyToOne
    private Project project;

    @ManyToOne
    private Employee employee;

    @ManyToOne
    private Sprint sprint;

}
