package com.ksundaysky.agileboard.repositories;

import com.ksundaysky.agileboard.domain.Employee;
import com.ksundaysky.agileboard.domain.Team;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface EmployeeRepository extends CrudRepository<Employee,Long> {

    Employee findByEmail(String email);

}
