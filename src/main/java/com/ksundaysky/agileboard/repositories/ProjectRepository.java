package com.ksundaysky.agileboard.repositories;

import com.ksundaysky.agileboard.domain.Project;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProjectRepository extends CrudRepository<Project,Long> {

    Optional<Project> findByName(String name);
}
