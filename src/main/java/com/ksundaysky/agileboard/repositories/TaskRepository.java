package com.ksundaysky.agileboard.repositories;

import com.ksundaysky.agileboard.domain.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TaskRepository extends CrudRepository<Task,Long> {

    Optional<Task> findByName(String name);
}
