package com.ksundaysky.agileboard.repositories;

import com.ksundaysky.agileboard.domain.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TeamRepository extends CrudRepository<Team,Long>
{
    Optional<Team> findByName(String name);
}
