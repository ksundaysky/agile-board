//package com.ksundaysky.agileboard.repositories;
//
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//
//import java.util.List;
//
//public interface EmployeeTeamRepository extends CrudRepository<Long,Long>{
//
//    @Query("select s.employee_id from EMPLOYEE_TEAM s where s.team_id = ?1 ")
//    List<Long> findByTeamId(Long teamId);
//}
