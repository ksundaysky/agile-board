package com.ksundaysky.agileboard.repositories;

import com.ksundaysky.agileboard.domain.Sprint;
import org.springframework.data.repository.CrudRepository;

public interface SprintRepository extends CrudRepository<Sprint, Long> {

    Sprint findByName(String name);
}
